<?php

if (!function_exists("dump")) {

    /**
     * 
     * @return type
     */
    function dump() {
        return trigger_error("Function Deprecated, use knife::dump()!", E_USER_WARNING);
    }

}