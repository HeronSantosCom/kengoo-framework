<?php

if (!function_exists("mimetype")) {

    /**
     * 
     * @param type $filename
     * @return boolean
     */
    function mimetype($filename) {
        $pathinfo = pathinfo($filename);
        if (!empty($pathinfo['extension'])) {
            return (isset($_SESSION["__mimetypes__"][$pathinfo['extension']]) ? $_SESSION["__mimetypes__"][$pathinfo['extension']] : "application/octet-stream");
        }
        return false;
    }

}