<?php

if (!function_exists("__autoload")) {

    /**
     * 
     * @param type $class_name
     * @return boolean
     */
    function __autoload($class_name) {
        //$class_name = strtolower($class_name);
        if (!class_exists($class_name, false)) {
            $kernel = path::kernel("{$class_name}.php");
            if (file_exists($kernel)) {
                include $kernel;
                if (class_exists($class_name, false)) {
                    return true;
                }
            }
            $extensions = path::extensions("{$class_name}.php");
            if (file_exists($extensions)) {
                include $extensions;
                if (class_exists($class_name, false)) {
                    return true;
                }
            }
            $applications = path::applications("{$class_name}.php");
            if (file_exists($applications)) {
                include $applications;
                if (class_exists($class_name, false)) {
                    return true;
                }
            }
//            $applications = path::applications("{$class_name}/{$class_name}.php");
//            if (file_exists($applications)) {
//                include $applications;
//                if (class_exists($class_name)) {
//                    return true;
//                }
//            }
        }
    }

}