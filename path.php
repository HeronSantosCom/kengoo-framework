<?php

if (!class_exists('path')) {

    /**
     *
     */
    class path {

        /**
         *
         * @param type $filename
         * @return type
         */
        static function rtrim($filename) {
            $divisor = "\\";
            if (unix) {
                $divisor = "/";
            }
            if (substr($filename, (strlen($filename) - 1)) == $divisor) {
                $filename = substr($filename, 0, (strlen($filename) - 1));
            }
            return $filename;
        }

        /**
         *
         * @param type $filename
         * @return type
         */
        static function ltrim($filename) {
            $divisor = "\\";
            if (unix) {
                $divisor = "/";
            }
            if (substr($filename, 0, 1) == $divisor) {
                $filename = substr($filename, 1);
            }
            return $filename;
        }

        /**
         *
         * @param type $filename
         * @return boolean
         */
        static function concat($filename) {
            if (strlen($filename) > 0) {
                $filename = self::arrange($filename);
                return (unix ? "/{$filename}" : "\\{$filename}");
            }
            return false;
        }

        /**
         *
         * @param type $filename
         * @return type
         */
        static function arrange($filename) {
            if (unix) {
                return str_replace("\\", "/", $filename);
            }
            return str_replace("/", "\\", $filename);
        }

        /**
         *
         * @param type $filename
         * @return boolean
         */
        static function is_exists($filename) {
            if (file_exists($filename)) {
                return true;
            }
            return false;
        }

        /**
         *
         * @param type $directory
         * @param type $recursive
         * @param type $fullpath
         * @return type
         */
        static function scandir($directory, $recursive = false, $fullpath = false) {
            $array = false;
            $dir = self::sources($directory);
            if ($dir) {
                $files = array_diff(scandir($dir), array(".", "..", ".svn"));
                foreach ($files as $file) {
                    $file = "{$directory}/{$file}";
                    if (is_dir(self::sources($file))) {
                        if ($recursive) {
                            $dir = self::scandir(self::dir($file));
                            if ($dir) {
                                if (is_array($array)) {
                                    $array = array_merge($array, $dir);
                                } else {
                                    $array = $dir;
                                }
                            }
                        }
                    } else {
                        $array[] = ($fullpath ? self::sources($file) : $file);
                    }
                }
            }
            return $array;
        }

        /**
         *
         * @param type $filename
         * @return type
         */
        static function root($filename = false) {
            return realpath(dirname . self::concat($filename));
        }

        /**
         *
         * @param type $filename
         * @return type
         */
        static function kernel($filename = false) {
            return realpath(kernel_dirname . self::concat($filename));
        }

        /**
         *
         * @param type $filename
         * @return type
         */
        static function extensions($filename = false) {
            return self::kernel(extensions . self::concat($filename));
        }

        /**
         *
         * @param type $filename
         * @return type
         */
        static function template($filename = false) {
            return self::kernel(template . self::concat($filename));
        }

        /**
         *
         * @param type $filename
         * @return type
         */
        static function includes($filename = false) {
            return self::kernel(includes . self::concat($filename));
        }

        /**
         *
         * @param type $filename
         * @return type
         */
        static function plugins($filename = false) {
            return self::kernel(plugins . self::concat($filename));
        }

        /**
         *
         * @param type $filename
         * @return type
         */
        static function applications($filename = false) {
            return realpath(applications . self::concat($filename));
        }

        /**
         *
         * @param type $filename
         * @return type
         */
        static function sources($filename = false) {
            return realpath(sources . self::concat($filename));
        }

        /**
         *
         * @param type $filename
         * @return type
         */
        static function logs($filename = false) {
            return realpath(logs);
        }

        /**
         *
         * @param type $filename
         * @return type
         */
        static function file($filename = false) {
            $file = "index.html";
            $filename = ($filename ? $filename : self::get());
            $path = explode((unix ? "/" : "\\"), $filename);
            if (count($path) > 0) {
                if (is_file($filename)) {
                    $file = $path[(count($path) - 1)];
                }
            }
            return "/" . self::ltrim($file);
        }

        /**
         *
         * @param type $filename
         * @return type
         */
        static function dir($filename = false) {
            $filename = ($filename ? $filename : self::get());
            $path = explode((unix ? "/" : "\\"), $filename);
            if (count($path) > 0) {
                $check = implode((unix ? "/" : "\\"), $path);
                if (!is_dir($check)) {
                    $path = array_slice($path, 0, (count($path) - 1));
                }
            }
            return self::rtrim(implode((unix ? "/" : "\\"), $path));
        }

        /**
         *
         * @param type $filename
         * @return type
         */
        static function get($filename = false) {
            $path = ($filename ? $filename : self::arrange(self::sources() . uri::get()));
            if (!self::is_exists($path)) {
                if (permalink) {
                    permalink::init($path);
                    $separador = (unix ? "/" : "\\");
                    $path = explode($separador, $path);
                    if (!mimetype($path[(count($path) - 1)])) {
                        if (count($path) > 0) {
                            $filename = implode($separador, array_slice($path, 0, (count($path) - 1)));
                            return self::get($filename);
                        }
                    }
                    $path = implode($separador, $path);
                }
            }
            permalink::set($path);
            return $path;
        }

    }

}