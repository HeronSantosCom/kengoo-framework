<?php

if (!class_exists('source')) {

    /**
     * 
     */
    class source {

        public $display = false;

        /**
         *
         */
        public function __construct() {
            if (!maintenance) {
                if (status) {
                    if (permalink) {
                        $_SESSION["_curte"]['filename'] = false;
                    }
                    $filename = path::dir() . path::file();
                    if (path::is_exists($filename)) {
                        if (!permalink) {
                            $uri_correct = uri::root() . uri::dir() . path::file();
                            $uri_entered = uri::root() . uri::get();
                            if ($uri_correct != $uri_entered) {
                                knife::redirect("$uri_correct" . uri::args());
                            }
                        }
                        $this->display = $this->cutout($filename);
                    } else {
                        $this->display = $this->notfound($filename);
                    }
                } else {
                    $this->display = $this->offline();
                }
            } else {
                $this->display = $this->maintenance();
            }
        }

        /**
         *
         * @param type $filename
         * @return type
         */
        private function cutout($filename) {
            $mimetype = mimetype($filename);
            if (strlen($mimetype) > 0) {
                header("Cache-Control: public, max-age=31536000");
                header("Expires : A31536000");
                switch ($mimetype) {
                    case "application/x-httpd-php":
                        return $this->open_php($filename);
                        break;
                    case "text/html":
                    case "application/xml":
                    case "application/xml":
                    case "application/atom+xml":
                        return $this->open_text($filename, $mimetype);
                        break;
                    case "text/css":
                    case "text/javascript":
                    case "image/png":
                    case "image/gif":
                    case "image/jpeg":
                    case "image/x-ico":
                    case "image/x-ico":
                    case "application/x-javascript":
                    case "application/x-shockwave-flash":
                        return $this->open_common($filename, $mimetype);
                        break;
                    default:
                        return $this->open_file($filename, $mimetype);
                        break;
                }
            }
            return $this->notfound($filename);
        }

        /**
         *
         * @param type $filename
         * @return type
         */
        private function open_php($filename) {
            header("Content-type: text/html; charset=" . encoding_application);
            $compose = new compose();
            ob_start();
            include $filename;
            return $compose->init(ob_get_clean());
        }

        /**
         *
         * @param type $filename
         * @param type $mimetype
         * @return type
         */
        private function open_text($filename, $mimetype) {
            header("Content-type: {$mimetype}; charset=" . encoding_application);
            $compose = new compose();
            return $compose->init(file_get_contents($filename));
        }

        /**
         * Content Cached Script by Ernie Leseberg (http://ernieleseberg.com/php-image-output-and-browser-caching/)
         * @param type $filename
         * @param type $mimetype
         */
        private function open_common($filename, $mimetype) {
            $filemtime = filemtime($filename);
            // Getting headers sent by the client.
            $headers = $this->getRequestHeaders();
            // Checking if the client is validating his cache and if it is current.
            if (isset($headers['If-Modified-Since']) && (strtotime($headers['If-Modified-Since']) == $filemtime)) {
                // Client's cache IS current, so we just respond '304 Not Modified'.
                header('Last-Modified: ' . gmdate('D, d M Y H:i:s', $filemtime) . ' GMT', true, 304);
            } else {
                // Image not cached or cache outdated, we respond '200 OK' and output the image.
                header('Last-Modified: ' . gmdate('D, d M Y H:i:s', $filemtime) . ' GMT', true, 200);
                header('Content-type: ' . $mimetype);
                header('Content-transfer-encoding: binary');
                header('Content-length: ' . filesize($filename));
                readfile($filename);
            }
            exit;
        }

        /**
         *
         * @return type
         */
        private function getRequestHeaders() {
            if (function_exists("apache_request_headers")) {
                if ($headers = apache_request_headers()) {
                    return $headers;
                }
            }
            $headers = array();
            // Grab the IF_MODIFIED_SINCE header
            if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE'])) {
                $headers['If-Modified-Since'] = $_SERVER['HTTP_IF_MODIFIED_SINCE'];
            }
            return $headers;
        }

        /**
         *
         * @param type $filename
         * @param type $mimetype
         */
        private function open_file($filename, $mimetype) {
            header("Pragma: public");
            header("Expires: Off");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Cache-Control: private", false);
            header("Content-Type: {$mimetype}");
            header("Content-Transfer-Encoding: binary");
            header("Content-Length: " . filesize($filename));
            flush();
            readfile($filename);
            exit(0);
        }

        /**
         *
         * @return type
         */
        private function maintenance() {
            header("HTTP/1.0 503 Service Unavailable");
            header("Status: 503 Service Unavailable");
            $_SERVER['REDIRECT_STATUS'] = 503;
            return $this->open_text(path::template("maintenance.html"), "text/html");
        }

        /**
         *
         * @return type
         */
        private function offline() {
            header("HTTP/1.0 503 Service Unavailable");
            header("Status: 503 Service Unavailable");
            $_SERVER['REDIRECT_STATUS'] = 503;
            return $this->open_text(path::template("offline.html"), "text/html");
        }

        /**
         *
         * @param type $filename
         * @return type
         */
        private function notfound($filename) {
            header("HTTP/1.0 404 Not Found");
            header("Status: 404 Not Found");
            $_SERVER['REDIRECT_STATUS'] = 404;
            return $this->open_text(path::template("notfound.html"), "text/html");
        }

        /**
         *
         * @param type $filename
         * @return type
         */
        private function redirect($filename) {
            header("location: {$filename}");
            return exit(1);
        }

    }

}