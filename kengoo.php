<?php

if (!class_exists('kengoo')) { 

    /**
     * 
     */
    class kengoo {

        private $__FILE__;

        /**
         * 
         * @param type $__FILE__
         */
        public function __construct($__FILE__) {
            $this->__FILE__ = $__FILE__;
            setlocale(LC_ALL, "pt_BR");
            date_default_timezone_set("America/Sao_Paulo");
            define("sysname", "kenGoo");
            define("sysversion", "1.5.0");
            define("sysbuild", "2013-08-10");
            define("sys", sysname . "/" . sysversion . " (build " . sysbuild . ")");

            if ($this->provision()) {
                try {
                    new boot();
                } catch (Exception $exc) {
                    knife::dump("Stop Invoked Exception: {$exc->getMessage()}", "{$exc->getTraceAsString()}");
                }
            }
        }

        /**
         * 
         * @return boolean
         */
        private function provision() {
            $domain = "localhost";
            if (isset($_SERVER["HTTP_HOST"])) {
                $domain = $_SERVER["HTTP_HOST"];
                if (substr($domain, 0, 4) == "www.") {
                    $domain = substr($domain, 4);
                }
                if (substr($domain, -1) == ".") {
                    $domain = substr($domain, 0, -1);
                }
                if (strpos($domain, ":")) {
                    $domain = explode(":", $domain, 2);
                    $domain = $domain[0];
                }
            }
            define("domain", strtolower($domain));
            define("instance", md5(domain . uniqid()));
            define("ajax", (isset($_SERVER['HTTP_X_REQUESTED_WITH']) and strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'));
            define("configurations", "config.xml");
            define("service", "service.php");
            define("htaccess", ".htaccess");
            define("extensions", "extensions");
            define("includes", "includes");
            define("template", "template");
            define("plugins", "plugins");
            define("applications", "applications");
            define("sources", "sources");
            define("logs", "logs");
            if ($this->root() and $this->kernel() and $this->path() and $this->autoload() and $this->error() and $this->dump()) {
                return $this->mimetype();
            }
            return false;
        }

        /**
         * 
         * @return boolean
         */
        private function root() {
            $pathinfo = pathinfo($this->__FILE__);
            if (isset($pathinfo["dirname"])) {
                define("unix", (substr($pathinfo["dirname"], 1, 2) == ":\\" ? false : true));
                define("dirname", $pathinfo["dirname"]);
                return true;
            }
            return false;
        }

        /**
         * 
         * @return boolean
         */
        private function kernel() {
            $pathinfo = pathinfo(__FILE__);
            if (isset($pathinfo["dirname"])) {
                define("kernel_unix", (substr($pathinfo["dirname"], 1, 2) == ":\\" ? false : true));
                define("kernel_dirname", $pathinfo["dirname"]);
                return true;
            }
            return false;
        }

        /**
         * 
         * @return type
         */
        private function path() {
            include 'path.php';
            return class_exists("path");
        }

        /**
         * 
         * @return type
         */
        private function autoload() {
            include includes . '/__autoload.php';
            if (function_exists("__autoload")) {
                return spl_autoload_register("__autoload");
            }
            return false;
        }

        /**
         * 
         * @return boolean
         */
        private function error() {
            include includes . '/error.php';
            if (function_exists("error")) {
//            $filename = path::logs() . path::concat(date("Ymd") . ".log");
//            if (file_exists($filename)) {
//                if (filesize($filename) > 2097152) {
//                    $dest = path::logs() . path::concat(date("Ymd") . "-" . getmypid() . ".log");
//                    rename($filename, $dest);
//                }
//            }
                ini_set('error_reporting', E_ALL);
                ini_set("display_errors", true);
//            ini_set('error_log', $filename);
                set_error_handler('error');
                register_shutdown_function('error');
                return true;
            }
            return false;
        }

        /**
         * 
         * @return type
         */
        private function dump() {
            include includes . '/dump.php';
            return function_exists("dump");
        }

        /**
         * 
         * @return type
         */
        private function mimetype() {
            include includes . '/mimetype.php';
            return function_exists("mimetype");
        }

    }

}