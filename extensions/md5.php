<?php

if (!class_exists('md5')) {

    /**
     *
     */
    class md5
    {

        private $iv_len, $password, $certificate;

        /**
         *
         * @param type $iv_len
         */
        public function __construct($iv_len = 16)
        {
            $this->password = "αβγδεζηθικλμνξορςστυφχψωΑΒΓΕΖΗΘΙΚΜΛΝΞΟΠΡΣΤΥΦΧΨΩΣπℎℏΔ≤≥×∑∫±";
            $this->iv_len = $iv_len;
            $this->certificate = 'md5Crypt';
        }

        /**
         *
         * @param type $iv_len
         * @return type
         */
        private function get_rnd_iv($iv_len)
        {
            $iv = null;
            while ($iv_len-- > 0) {
                $iv .= chr(mt_rand() & 0xff);
            }
            return $iv;
        }

        /**
         *
         * @param type $value
         * @return type
         */
        public function set($value)
        {
            $value = serialize($value);
            $value .= "\x13";
            $n = strlen($value);
            if ($n % 16)
                $value .= str_repeat("\0", 16 - ($n % 16));
            $i = 0;
            $enc_text = $this->get_rnd_iv($this->iv_len);
            $iv = substr($this->password ^ $enc_text, 0, 512);
            while ($i < $n) {
                $block = substr($value, $i, 16) ^ pack('H*', md5($iv));
                $enc_text .= $block;
                $iv = substr($block . $iv, 0, 512) ^ $this->password;
                $i += 16;
            }
            return strtr(base64_encode($this->certificate . $enc_text), '+/=', '-_.');
//            return base64_encode($enc_text);
        }

        /**
         *
         * @param type $hash
         * @return type
         */
        public function get($hash)
        {
            $hash = base64_decode(strtr($hash, '-_.', '+/='));
            if (substr($hash, 0, strlen($this->certificate)) === $this->certificate) {
                $hash = substr($hash, strlen($this->certificate));
                $n = strlen($hash);
                $i = $this->iv_len;
                $plain_text = '';
                $iv = substr($this->password ^ substr($hash, 0, $this->iv_len), 0, 512);
                while ($i < $n) {
                    $block = substr($hash, $i, 16);
                    $plain_text .= $block ^ pack('H*', md5($iv));
                    $iv = substr($block . $iv, 0, 512) ^ $this->password;
                    $i += 16;
                }
                return @unserialize(preg_replace('/\\x13\\x00*$/', '', $plain_text));
            }
            return false;
        }

    }

}