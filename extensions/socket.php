<?php

if (!class_exists('socket')) {

    /**
     *
     */
    class socket {

        protected $server, $port, $timeout;
        public $link;

        /**
         *
         * @param type $server
         * @param type $port
         * @param type $timeout
         */
        public function __construct($server, $port = 25, $timeout = 45) {
            $this->server = $server;
            $this->port = $port;
            $this->timeout = $timeout;
        }

        /**
         *
         */
        public function __destruct() {
            $this->close();
        }

        /**
         *
         * @return boolean
         */
        public function open() {
            $this->link = @fsockopen($this->server, $this->port, $errno, $errstr, $this->timeout);
            if ($this->link) {
                knife::dump("Socket: Connected at {$this->server}:{$this->port}...");
                socket_set_timeout($this->link, 0, ($this->timeout * 1000));
                return $this->link;
            }
            trigger_error($errstr);
            return false;
        }

        /**
         *
         * @param string $command
         * @return boolean
         */
        public function put($command) {
            if (is_resource($this->link)) {
                $command .= "\r\n";
                $response = fputs($this->link, $command, strlen($command) + 2);
                if ($response) {
                    knife::dump("Socket Put: {$command}");
                    return $response;
                }
            }
            return false;
        }

        /**
         *
         * @param type $put
         * @param type $break
         * @return boolean
         */
        public function get($put = false, $break = false) {
            if ($put) {
                if (!$this->put($put)) {
                    return false;
                }
            }
            if (is_resource($this->link)) {
                $continue = true;
                while ($continue) {
                    $response[] = $parser = fgets($this->link);
                    knife::dump("Socket Get: {$parser}");
                    if (!$break) {
                        if (!(strpos($parser, "\r\n") == false or substr($parser, 3, 1) != ' ')) {
                            $continue = false;
                        }
                    } else {
                        if ($parser == $break) {
                            $continue = false;
                        }
                    }
                }
                if (isset($response)) {
                    return $response;
                }
            }
            return false;
        }

        /**
         *
         * @return boolean
         */
        public function close() {
            if (is_resource($this->link)) {
                if (fclose($this->link)) {
                    knife::dump("Socket: Disconnected from {$this->server}:{$this->port}...");
                    return true;
                }
            }
            return false;
        }

    }

}