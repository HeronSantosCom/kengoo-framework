<?php

if (!class_exists('mysqlwhere')) {

    /**
     * Esta versão será substituida pela versão única do mysql a ser construída.
     * Contará com algumas melhorias e implementações que constam nesta classe.
     */
    class mysqlwhere extends mysql {

        /**
         *
         */
        public function __construct() {
            parent::__construct();
        }

        /**
         *
         * @param string $column
         * @param type $value
         * @param type $glue
         * @param type $inverse
         * @param type $tablekey
         * @return boolean
         */
        public function match($column, $value, $glue = false, $inverse = false, $tablekey = 1) {
            if (!$this->is_function($column)) {
                if (!isset($this->table[$tablekey])) {
                    trigger_error("Requested table unreported!");
                    return false;
                }
                $column = "{$this->table[$tablekey][2]}." . $this->ereaser($column, "`", false);
            }
            $value = trim($value);
            if (!$this->is_function($value)) {
                $value = (strlen($value) > 0 ? "\"" . mysqli_real_escape_string($this->link, $value) . "\"" : "NULL");
            }
            $inverse = ($inverse ? "<>" : "=");
            return $this->where(($this->where ? ($glue ? "{$glue} " : "AND ") : null) . "{$column} {$inverse} {$value}");
        }

        /**
         *
         * @param string $column
         * @param type $value
         * @param type $glue
         * @param type $inverse
         * @param type $tablekey
         * @return boolean
         */
        public function between($column, $value, $glue = false, $inverse = false, $tablekey = 1) {
            if (!$this->is_function($column)) {
                if (!isset($this->table[$tablekey])) {
                    trigger_error("Requested table unreported!");
                    return false;
                }
                $column = "{$this->table[$tablekey][2]}." . $this->ereaser($column, "`", false);
            }
            if (is_array($value) and count($value) == 2) {
                foreach ($value as $key => $row) {
                    $row = trim($row);
                    if (!$this->is_function($row)) {
                        $row = (strlen($row) > 0 ? "\"" . mysqli_real_escape_string($this->link, $row) . "\"" : "NULL");
                    }
                    $value[$key] = $row;
                }
                $value = join(" AND ", $value);
            } else {
                $value = trim($value);
                if (!$this->is_function($value)) {
                    $value = (strlen($value) > 0 ? "\"" . mysqli_real_escape_string($this->link, $value) . "\"" : "NULL");
                }
            }
            $inverse = ($inverse ? "NOT BETWEEN" : "BETWEEN");
            return $this->where(($this->where ? ($glue ? "{$glue} " : "AND ") : null) . "({$column} {$inverse} {$value})");
        }

        /**
         *
         * @param string $column
         * @param type $value
         * @param type $glue
         * @param type $inverse
         * @param type $tablekey
         * @return boolean
         */
        public function more($column, $value, $glue = false, $inverse = false, $tablekey = 1) {
            if (!$this->is_function($column)) {
                if (!isset($this->table[$tablekey])) {
                    trigger_error("Requested table unreported!");
                    return false;
                }
                $column = "{$this->table[$tablekey][2]}." . $this->ereaser($column, "`", false);
            }
            $value = trim($value);
            if (!$this->is_function($value)) {
                $value = (strlen($value) > 0 ? "\"" . mysqli_real_escape_string($this->link, $value) . "\"" : "NULL");
            }
            $inverse = ($inverse ? "<" : ">");
            return $this->where(($this->where ? ($glue ? "{$glue} " : "AND ") : null) . "{$column} {$inverse} {$value}");
        }

        /**
         *
         * @param string $column
         * @param type $value
         * @param type $glue
         * @param type $inverse
         * @param type $tablekey
         * @return boolean
         */
        public function morethan($column, $value, $glue = false, $inverse = false, $tablekey = 1) {
            if (!$this->is_function($column)) {
                if (!isset($this->table[$tablekey])) {
                    trigger_error("Requested table unreported!");
                    return false;
                }
                $column = "{$this->table[$tablekey][2]}." . $this->ereaser($column, "`", false);
            }
            $value = trim($value);
            if (!$this->is_function($value)) {
                $value = (strlen($value) > 0 ? "\"" . mysqli_real_escape_string($this->link, $value) . "\"" : "NULL");
            }
            $inverse = ($inverse ? "<=" : ">=");
            return $this->where(($this->where ? ($glue ? "{$glue} " : "AND ") : null) . "{$column} {$inverse} {$value}");
        }

        /**
         *
         * @param string $column
         * @param type $value
         * @param type $glue
         * @param type $inverse
         * @param type $tablekey
         * @return boolean
         */
        public function in($column, $value, $glue = false, $inverse = false, $tablekey = 1) {
            if (!$this->is_function($column)) {
                if (!isset($this->table[$tablekey])) {
                    trigger_error("Requested table unreported!");
                    return false;
                }
                $column = "{$this->table[$tablekey][2]}." . $this->ereaser($column, "`", false);
            }
            if (is_array($value)) {
                foreach ($value as $key => $aux) {
                    if (!$this->is_function($aux)) {
                        $aux = trim($aux);
                        $value[$key] = (strlen($aux) > 0 ? "\"" . mysqli_real_escape_string($this->link, $aux) . "\"" : "NULL");
                    }
                }
                $value = join(", ", $value);
            } else {
                $value = trim($value);
                if (!$this->is_function($value)) {
                    $value = (strlen($value) > 0 ? mysqli_real_escape_string($this->link, $value) : "NULL");
                }
            }
            $inverse = ($inverse ? "NOT IN" : "IN");
            return $this->where(($this->where ? ($glue ? "{$glue} " : "AND ") : null) . "{$column} {$inverse} ({$value})");
        }

        /**
         *
         * @param string $column
         * @param type $value
         * @param type $glue
         * @param type $inverse
         * @param type $tablekey
         * @return boolean
         */
        public function is($column, $value, $glue = false, $inverse = false, $tablekey = 1) {
            if (!$this->is_function($column)) {
                if (!isset($this->table[$tablekey])) {
                    trigger_error("Requested table unreported!");
                    return false;
                }
                $column = "{$this->table[$tablekey][2]}." . $this->ereaser($column, "`", false);
            }
            $value = trim($value);
            if (!$this->is_function($value)) {
                $value = (strlen($value) > 0 ? mysqli_real_escape_string($this->link, $value) : "NULL");
            }
            $inverse = ($inverse ? "IS NOT" : "IS");
            return $this->where(($this->where ? ($glue ? "{$glue} " : "AND ") : null) . "{$column} {$inverse} {$value}");
        }

        /**
         *
         * @param string $column
         * @param type $value
         * @param type $glue
         * @param type $inverse
         * @param type $tablekey
         * @return boolean
         */
        public function like($column, $value, $glue = false, $inverse = false, $tablekey = 1) {
            if (!$this->is_function($column)) {
                if (!isset($this->table[$tablekey])) {
                    trigger_error("Requested table unreported!");
                    return false;
                }
                $column = "{$this->table[$tablekey][2]}." . $this->ereaser($column, "`", false);
            }
            $value = trim($value);
            if (!$this->is_function($value)) {
                $value = (strlen($value) > 0 ? "\"" . mysqli_real_escape_string($this->link, $value) . "\"" : "NULL");
            }
            $inverse = ($inverse ? "NOT LIKE" : "LIKE");
            return $this->where(($this->where ? ($glue ? "{$glue} " : "AND ") : null) . "{$column} {$inverse} {$value}");
        }

        /**
         *
         * @param type $column
         * @param type $value
         * @param type $glue
         * @param type $inverse
         * @param type $tablekey
         * @return boolean
         */
        public function against($column, $value, $glue = false, $inverse = false, $tablekey = 1) {
            if (is_array($column)) {
                foreach ($column as $key => $col) {
                    $table = (is_array($tablekey) ? (isset($tablekey[$key]) ? $tablekey[$key] : $tablekey[0] ) : $tablekey);
                    if (!$this->is_function($col)) {
                        if (!isset($this->table[$table])) {
                            trigger_error("Requested table unreported!");
                            return false;
                        }
                        $column[$key] = "{$this->table[$tablekey][2]}." . $this->ereaser($col, "`", false);
                    }
                }
                $column = join(", ", $column);
            } else {
                if (!$this->is_function($column)) {
                    if (!isset($this->table[$tablekey])) {
                        trigger_error("Requested table unreported!");
                        return false;
                    }
                    $column = "{$this->table[$tablekey][2]}." . $this->ereaser($column, "`", false);
                }
            }
            $value = trim($value);
            if (!$this->is_function($value)) {
                $value = (strlen($value) > 0 ? "\"" . mysqli_real_escape_string($this->link, $value) . "\"" : "NULL");
            }
            $inverse = ($inverse ? "NOT MATCH" : "MATCH");
            $alias = "r{$this->index("relevance")}";
            //  AS 'r1'
            //$this->column("FLOOR((CONVERT({$inverse} ({$column}) AGAINST ({$value} IN BOOLEAN MODE), DECIMAL)*100) / AVG(LENGTH(CONCAT_WS(' ',{$column})) - LENGTH(REPLACE(CONCAT_WS(' ',{$column}), ' ', '')) + 1))", false, $alias);
            $this->column("{$inverse} ({$column}) AGAINST ({$value} IN BOOLEAN MODE)", false, $alias);
            //$this->column("{$inverse} ({$column}) AGAINST ({$value} IN BOOLEAN MODE)", false, $alias);
            //$this->order(count($this->columns), "DESC");
            return $this->where(($this->where ? ($glue ? "{$glue} " : "AND ") : null) . "{$inverse} ({$column}) AGAINST ({$value} IN BOOLEAN MODE)");
        }

        /**
         *
         * @param type $conditions
         * @return \mysqlwhere
         */
        public function where($conditions) {
            $index = $this->index("where");
            $this->where[$index] = $conditions;
            return $this;
        }

    }

}