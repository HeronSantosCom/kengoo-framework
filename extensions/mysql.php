<?php

if (!class_exists('mysql')) {

    /**
     * Esta versão será substituida pela versão orientada a objeto da classe nativa do PHP.
     * Contará com algumas melhorias e implementações que constam nesta classe.
     */
    class mysql {

        protected $server, $username, $password, $select, $maxlimit, $timezone, $encoding, $link, $dump;
        private $query = false;
        public $method = false;
        public $database = false;
        public $table = false;
        public $columns = false;
        public $where = false;
        public $group = false;
        public $having = false;
        public $order = false;
        public $limit = false;
        public $result = false;
        public $index = false;

        /**
         *
         * @param type $server
         * @param type $username
         * @param type $password
         * @param type $select
         * @param type $maxlimit
         * @param type $timezone
         * @param type $encoding
         */
        public function __construct($server = mysql_server, $username = mysql_username, $password = mysql_password, $select = mysql_database, $maxlimit = mysql_limit, $timezone = timezone_unix, $encoding = encoding_application) {
            $this->server = $server;
            $this->username = $username;
            $this->password = $password;
            $this->select = $select;
            $this->maxlimit = $maxlimit;
            $this->timezone = $timezone;
            $this->encoding = $encoding;
            $this->locale = setlocale(LC_ALL, 0);
            $this->dump = (defined("mysql_dump") ? mysql_dump : false);
            $this->connect();
        }

        /**
         *
         * @return boolean
         */
        public function connect() {
            if (!is_resource($this->link)) {
                $this->link = mysqli_connect($this->server, $this->username, $this->password, $this->select);
                if ($this->link) {
//                    if (mysqli_select_db($this->select, $this->link)) {
                    if ($this->encoding) {
//                            if (mysqli_client_encoding() != $this->encoding) {
                        if (!mysqli_set_charset($this->link, $this->encoding)) {
                            mysqli_query($this->link, "SET NAMES {$this->encoding}");
                        }
//                            }
                    }
                    if ($this->timezone) {
                        mysqli_query($this->link, "SET SESSION time_zone = '{$this->timezone}'");
                    }
                    if ($this->locale) {
                        mysqli_query($this->link, "SET SESSION lc_time_names = '{$this->locale}'");
                    }
                    return true;
//                    }
//                    trigger_error("Unable to select database {$this->select} in {$this->username}@{$this->server}.");
//                    return false;
                }
            }
            trigger_error("Could not connect to database server {$this->username}@{$this->server}.");
            return false;
        }

        /**
         *
         * @return boolean
         */
        public function disconnect() {
            if (is_resource($this->link)) {
                if (mysqli_close($this->link)) {
                    knife::dump("MySQL: Disconnected from {$this->username}@{$this->server}");
                    return true;
                }
            }
            return false;
        }

        /**
         *
         * @param type $query
         * @return \mysql
         */
        public function queue($query) {
            $this->query[] = $query;
            return $this;
        }

        /**
         *
         * @param type $query
         * @return boolean
         */
        public function commit($query = false) {
            if (!$query) {
                if (!is_array($this->query)) {
                    trigger_error("No command to run!");
                    return false;
                }
                $query = join("; ", $this->query);
                $this->query = false;
            }
            $t_start = array_sum(explode(' ', microtime()));
            $result["result"] = mysqli_query($this->link, $query);
            $result["rows"] = mysqli_affected_rows($this->link);
            $result["id"] = mysqli_insert_id($this->link);
            $result["error"] = mysqli_error($this->link);
            if (preg_match('/SELECT SQL_CALC_FOUND_ROWS/i', $query)) {
                $FOUND_ROWS = mysqli_fetch_assoc(mysqli_query($this->link, 'SELECT FOUND_ROWS() AS `TOTAL`'));
                $result["rows"] = array('founds' => $FOUND_ROWS['TOTAL'], 'displayed' => $result["rows"]);
            }
            $this->dump($query, $result["rows"], $result["id"], $result["error"], $t_start);
            return $result;
        }

        /**
         *
         * @param type $table
         * @return boolean
         */
        public function primarykey($table) {
            $return = $this->commit("SHOW /*!32332 FULL */ COLUMNS FROM `{$table}` WHERE `KEY` = 'PRI'");
            if ($return["result"]) {
                if (@mysqli_num_rows($return["result"]) > 0) {
                    $SHOW_COLUMNS = mysqli_fetch_assoc($return["result"]);
                    return $SHOW_COLUMNS;
                }
            }
            return false;
        }

        /**
         *
         * @param type $value
         * @param type $capsulate
         * @param type $exception
         * @return boolean
         */
        protected function ereaser($value, $capsulate = false, $exception = false) {
            $function = self::is_function($value);
            if ($function) {
                if ($capsulate and !$exception) {
                    trigger_error("You can not encapsulate the function {$function}.");
                    return false;
                }
            } else {
                $value = trim(str_replace(array('"', "'", "`"), "", $value));
                if (strlen($value) > 0 and $capsulate) {
                    $value = "{$capsulate}{$value}{$capsulate}";
                }
            }
            return $value;
        }

        /**
         *
         * @param type $value
         * @return boolean
         */
        protected function is_function($value) {
            $position = trim(strpos($value, "("));
            if ($position > 0) {
                $function = strtoupper(trim(substr($value, 0, $position)));
                switch ($function) {
                    case "MATCH":
                    case "NOT MATCH":
                    case "COUNT":
                    case "AVG":
                    case "SUM":
                    case "CONCAT":
                    case "CONCAT_WS":
                    case "IF":
                    case "MAX":
                    case "MIN":
                    case "MD5":
                    case "DATE":
                    case "DATE_FORMAT":
                    case "LENGTH":
                    case "FLOOR":
                    case "CONVERT":
                    case "CEIL":
                    case "MONTH":
                    case "YEAR":
                        return $function;
                        break;
                }
            }
            return false;
        }

        /**
         *
         * @param type $object
         * @param type $get
         * @return boolean
         */
        protected function index($object, $get = false) {
            if ($get) {
                if (isset($this->index[$object])) {
                    return $this->index[$object];
                }
                return false;
            }
            return $this->index[$object] = (isset($this->index[$object]) ? $this->index[$object] + 1 : 1);
        }

        /**
         *
         * @return mixed
         */
        protected function rows() {
            if (isset($this->result["rows"])) {
                return $this->result["rows"];
            }
            return false;
        }

        /**
         *
         * @param type $query
         * @param type $rows
         * @param type $id
         * @param type $error
         * @param type $t_start
         */
        protected function dump($query, $rows, $id, $error, $t_start) {
            $t_end = ((ceil((array_sum(explode(' ', microtime())) - $t_start) * 10000)) / 10000);
            if (strlen($error) > 0) {
                knife::dump("MySQL Query: {$query}", "MySQL Error: {$error}", "MySQL Time: {$t_end}s", str_pad("", 80, "="));
            } else {
                if ($this->dump === true) {
                    knife::dump("MySQL Query: {$query}", "MySQL Last Id: {$id}", "MySQL Rowns: " . (is_array($rows) ? "{$rows['displayed']}/{$rows['founds']}" : $rows), "MySQL Time: {$t_end}s", str_pad("", 80, "="));
                }
            }
        }

    }

}
