<?php

if (!class_exists('mysqlshow')) {

    /**
     * Esta versão será substituida pela versão única do mysql a ser construída.
     * Contará com algumas melhorias e implementações que constam nesta classe.
     */
    class mysqlshow extends mysql {

        /**
         *
         */
        public function __construct() {
            parent::__construct();
        }

        /**
         *
         * @param type $name
         * @return \mysqlshow
         */
        public function database($name) {
            $index = $this->index("database");
            $this->database[$index] = $this->ereaser($name, "`");
            return $this;
        }

        /**
         *
         * @param type $name
         * @param type $databasekey
         * @param type $alias
         * @return \mysqlshow
         */
        public function table($name, $databasekey = 1, $alias = false) {
            $database = (isset($this->database[$databasekey]) ? "{$this->database[$databasekey]}." : $this->ereaser($this->select, "`") . ".");
            $table = $this->ereaser($name, "`");
            $index = $this->index("table");
            $alias = ($alias ? $alias : $this->ereaser("t{$index}", "`"));
            $this->table[$index] = array(false, "{$database}{$table} {$alias}", $alias, "{$database}{$table}", $table);
            return $this;
        }

        /**
         *
         * @return boolean
         */
        public function columns($table = false) {
            $tables = ($table ? array(0 => array(3 => $table)) : $this->table);
            if ($tables) {
                $search = false;
                foreach ($tables as $value) {
                    $result = $this->commit("SHOW /*!32332 FULL */ COLUMNS FROM {$value[3]}");
                    if ($result["result"]) {
                        if (@mysqli_num_rows($result["result"]) > 0) {
                            while ($line = mysqli_fetch_assoc($result["result"])) {
                                $search[] = $line;
                            }
                            mysqli_free_result($result["result"]);
                        }
                    }
                }
                return $search;
            }
            trigger_error("Table unreported!");
            return false;
        }

        /**
         *
         * @param type $object
         * @param type $get
         * @return boolean
         */
        protected function index($object, $get = false) {
            if ($get) {
                if (isset($this->index[$object])) {
                    return $this->index[$object];
                }
                return false;
            }
            return $this->index[$object] = (isset($this->index[$object]) ? $this->index[$object] + 1 : 1);
        }

    }

}