<?php

if (!class_exists('permalink')) {

    /**
     *
     */
    class permalink {

        /**
         *
         * @param type $path
         */
        public static function init($path) {
            if (!defined("permalink_path_full")) {
                define("permalink_path_full", $path);
            }
        }

        /**
         *
         * @param type $path
         */
        public static function set($path) {
            if (defined("permalink_path_full") and !defined("permalink_path_split")) {
                define("permalink_path_split", serialize(explode((unix ? "/" : "\\"), path::rtrim(path::ltrim(str_replace($path, "", permalink_path_full))))));
            }
        }

        /**
         *
         * @param type $index
         * @return boolean
         */
        public static function get($index = false) {
            if (defined("permalink_path_split")) {
                $split = unserialize(permalink_path_split);
                if ($index !== false and isset($split[$index])) {
                    return $split[$index];
                }
                return $split;
            }
            return false;
        }

    }

}