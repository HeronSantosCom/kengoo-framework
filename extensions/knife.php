<?php

if (!class_exists('knife')) {

    /**
     *
     */
    class knife {

        /**
         *
         * @param type $var
         * @return type
         */
        private static function log_printer($var, $type = E_NOTICE) { // , $parent
            if (!is_string($var)) {
                $var = explode("\n", var_export($var, true));
                foreach ($var as $row => $value) {
                    self::log_printer($value, $type); // , $parent
                }
                return;
            } else {
                if (preg_match("/\\n/", $var)) {
                    return self::log_printer(explode("\n", $var), $type); // , $parent
                }
            }
            $ip = (!empty($_SERVER["REMOTE_ADDR"]) ? $_SERVER["REMOTE_ADDR"] : "127.0.0.1");
//            switch ($type) {
//                case E_ERROR:
//                    $var = sprintf("\033[0;31m%s\033[0m", $var);
//                    break;
//                case E_WARNING:
//                    $var = sprintf("\033[0;33m%s\e[0m", $var);
//                    break;
//                case E_PARSE:
//                    $var = sprintf("\033[1;36m%s\e[0m", $var);
//                    break;
//                case E_STRICT:
//                    $var = sprintf("\033[41;1;37m%s\e[0m", $var);
//                    break;
//                default:
//                    $var = sprintf("\033[1;32m%s\033[0m", $var);
//                    break;
//            }
//            return file_put_contents($file, "[{$ip}] [" . getmypid() . "] " . $var . "\n", FILE_APPEND);
            return error_log("[{$ip}] [" . getmypid() . "] " . $var, 0); //  {$parent}
        }

        /**
         *
         * @param type $args
         */
        public static function dump($args) {
            $args = func_get_args();
            if (is_array($args)) {
                foreach ($args as $row => $var) {
                    self::log_printer($var); // , "[" . ++$row . "]"
                }
            }
        }

        /**
         *
         * @param type $call
         * @return boolean
         */
        public static function trace($call = false) {
            $debug = debug_backtrace();
            $last = array_shift($debug);
            $last_file = (!empty($last["file"]) ? $last["file"] : false);
            $last_line = (!empty($last["line"]) ? $last["line"] : false);
            if ($last_file && $last_line) {
                if ($call) {
                    $debug = array_reverse($debug);
                    foreach ($debug as $row) {
                        $row_file = (!empty($row["file"]) ? $row["file"] : false);
                        $row_line = (!empty($row["line"]) ? $row["line"] : false);
                        if ($row_file && $row_line && !preg_match("/\/servers\/ikaros\/kengoo/i", $row_file)) {
                            self::dump("|--> " . $row_file . ": " . $row_line);
                        }
                    }
                    return self::dump("|-----> " . $last_file . ": " . $last_line);
                }
                return self::dump($last_file . ": " . $last_line);
            }
            return false;
        }

        /**
         *
         * @param type $to
         * @param type $subject
         * @param type $message
         * @param type $header
         * @return type
         */
        public static function mail_utf8($to, $subject = '(No subject)', $message = '', $header = '') {
            $header_ = 'MIME-Version: 1.0' . "\r\n" . 'Content-type: text/plain; charset=UTF-8' . "\r\n";
            return mail($to, '=?UTF-8?B?' . base64_encode($subject) . '?=', $message, $header_ . $header);
        }

        /**
         *
         * @param type $url
         * @return type
         */
        public static function is_url($url) {
            return preg_match("#^http(s)?://[a-z0-9-_.]+\.[a-z]{2,4}#i", $url);
        }

        /**
         *
         * @param type $ip
         * @return type
         */
        public static function is_ip($ip) {
            return preg_match('/^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\z/', $ip);
        }

        /**
         * 
         * @param type $mail
         * @return type
         */
        public static function is_mail($mail) {
            return preg_match("/^[_a-z0-9-]+(\.[_a-z0-9+-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/i", $mail);
        }

        /**
         *
         * @param type $email
         * @param type $size
         * @return type
         */
        public static function gravatar($email, $size = 125) {
            return "http://www.gravatar.com/avatar/?gravatar_id=" . md5(strtolower(trim($email))) . "&size={$size}&default=" . urlencode("http://www.gravatar.com/avatar/ad516503a11cd5ca435acc9bb6523536?size={$size}");
        }

        /**
         *
         * @param type $filename
         * @return type
         */
        public static function application($filename = false) {
            return path::applications() . path::concat($filename);
        }

        /**
         *
         * @param type $filename
         * @return type
         */
        public static function source($filename = false) {
            return path::sources() . path::concat($filename);
        }

        /**
         *
         * @param type $filename
         * @return type
         */
        public static function path($filename = false) {
            return path::dir() . path::concat($filename);
        }

        /**
         *
         * @param type $filename
         * @param type $encode
         * @return boolean
         */
        public static function html($filename, $encode = true) {
            $filename = self::path($filename);
            if (path::is_exists($filename)) {
                header("Content-type: text/html; charset=" . encoding_application);
                $compose = new compose();
                $source = $compose->init(file_get_contents($filename), $encode);
                return $source;
            }
            return false;
        }

        /**
         *
         * @param type $url
         * @param type $curl
         * @param type $retry
         * @return string
         */
        public static function open($url, $curl = true, $retry = false) {
            $data = false;
            if ($curl) {
                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_TIMEOUT, 15);
                curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20041001 Firefox/0.10.1");
                //curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                ob_start();
                curl_exec($ch);
                curl_close($ch);
                $data = ob_get_contents();
                ob_end_clean();
            } else {
                ini_set('user_agent', 'Mozilla/5.0 (Windows; U; Windows NT 5.1; rv:1.7.3) Gecko/20041001 Firefox/0.10.1');
                $file = @fopen($url, 'r');
                if ($file) {
                    while (!feof($file)) {
                        $data = $data . @fgets($file, 4096);
                    }
                    fclose($file);
                }
            }
            if (preg_match('/301 Moved Permanently/i', $data)) {
                preg_match("/HREF=\"(.*)\"/si", $data, $r);
                return self::open($r[1], $curl);
            }
            if (!$retry and $curl and strlen($data) == 0) {
                return self::open($url, false, true);
            }
            return $data;
        }

        /**
         *
         * @param type $filename
         * @return type
         */
        public static function redirect($filename) {
            header("location: {$filename}");
            return exit(1);
        }

        /**
         *
         * @param type $date
         * @param type $delimiter
         * @param type $glue
         * @return type
         */
        public static function date_converter($date, $delimiter = "-", $glue = "/") {
            switch (strlen($date)) {
                case 10:
                    $date = explode($delimiter, $date);
                    return "{$date[2]}{$glue}{$date[1]}{$glue}{$date[0]}";
                    break;
                case 16:
                case 19:
                    $time = explode(" ", $date);
                    $date = explode($delimiter, $time[0]);
                    $time = $time[1];
                    return "{$date[2]}{$glue}{$date[1]}{$glue}{$date[0]} {$time}";
                    break;
                default:
                    return $date;
                    break;
            }
        }

        /**
         *
         * @param type $class
         * @param type $const
         * @return type
         */
        public static function get_class_const($class, $const) {
            return constant(sprintf('%s::%s', $class, $const));
        }

        /**
         *
         * @return type
         */
        public static function token() {
            $args = func_get_args();
            return md5(serialize($args));
        }

        /**
         *
         * @param type $string
         * @param type $strip_space
         * @return type
         */
        public static function remove_deadkeys($string, $strip_space = false) {
            $string = str_replace("Á", "A", $string);
            $string = str_replace("À", "A", $string);
            $string = str_replace("Â", "A", $string);
            $string = str_replace("Ã", "A", $string);
            $string = str_replace("Ä", "A", $string);
            $string = str_replace("á", "a", $string);
            $string = str_replace("à", "a", $string);
            $string = str_replace("â", "a", $string);
            $string = str_replace("ã", "a", $string);
            $string = str_replace("ä", "a", $string);
            $string = str_replace("É", "E", $string);
            $string = str_replace("È", "E", $string);
            $string = str_replace("Ê", "E", $string);
            $string = str_replace("Ë", "E", $string);
            $string = str_replace("é", "e", $string);
            $string = str_replace("è", "e", $string);
            $string = str_replace("ê", "e", $string);
            $string = str_replace("ë", "e", $string);
            $string = str_replace("É", "I", $string);
            $string = str_replace("Ì", "I", $string);
            $string = str_replace("Î", "I", $string);
            $string = str_replace("Ï", "I", $string);
            $string = str_replace("í", "i", $string);
            $string = str_replace("ì", "i", $string);
            $string = str_replace("î", "i", $string);
            $string = str_replace("ï", "i", $string);
            $string = str_replace("Ó", "O", $string);
            $string = str_replace("Ò", "O", $string);
            $string = str_replace("Ô", "O", $string);
            $string = str_replace("Õ", "O", $string);
            $string = str_replace("Ö", "O", $string);
            $string = str_replace("ó", "o", $string);
            $string = str_replace("ò", "o", $string);
            $string = str_replace("ô", "o", $string);
            $string = str_replace("õ", "o", $string);
            $string = str_replace("ö", "o", $string);
            $string = str_replace("Ú", "U", $string);
            $string = str_replace("Ù", "U", $string);
            $string = str_replace("Û", "U", $string);
            $string = str_replace("Ü", "U", $string);
            $string = str_replace("ú", "u", $string);
            $string = str_replace("ù", "u", $string);
            $string = str_replace("û", "u", $string);
            $string = str_replace("ü", "u", $string);
            $string = str_replace("¹", "1", $string);
            $string = str_replace("²", "2", $string);
            $string = str_replace("³", "3", $string);
            $string = str_replace("ª", "a", $string);
            $string = str_replace("Ç", "C", $string);
            $string = str_replace("ç", "c", $string);
            $string = str_replace("Ñ", "N", $string);
            $string = str_replace("ñ", "n", $string);
            if ($strip_space) {
                $string = str_replace("(", "_", $string);
                $string = str_replace(")", "_", $string);
                $string = str_replace("{", "_", $string);
                $string = str_replace("}", "_", $string);
                $string = str_replace("[", "_", $string);
                $string = str_replace("]", "_", $string);
                $string = str_replace("/", "_", $string);
                $string = str_replace("\\", "_", $string);
                $string = str_replace("$", "_", $string);
                $string = str_replace("@", "_", $string);
                $string = str_replace("!", "_", $string);
                $string = str_replace("&", "_", $string);
                $string = str_replace("%", "_", $string);
                $string = str_replace("?", "_", $string);
                $string = str_replace(";", "_", $string);
                $string = str_replace(",", "_", $string);
                $string = str_replace(" ", "_", $string);
                $string = str_replace("'", "_", $string);
                $string = str_replace('"', "_", $string);
            }
            return $string;
        }

        /**
         *
         * @param type $lsize
         * @param type $arredondado
         * @return type
         */
        public static function human_size_disk($lsize, $arredondado = false) {
            $unit = 0;
            $unit_type = 'Bytes';
            if ($lsize > pow(1024, 1)) {
                $unit_type = 'KBytes';
                $unit++;
            }
            if ($lsize > pow(1024, 2)) {
                $unit_type = 'MBytes';
                $unit++;
            }
            if ($lsize > pow(1024, 3)) {
                $unit_type = 'GBytes';
                $unit++;
            }
            if ($lsize > pow(1024, 4)) {
                $unit_type = 'TBytes';
                $unit++;
            }
            $lsize = ($lsize / pow(1024, $unit));
            return ($arredondado ? ceil($lsize) : number_format($lsize, 2, ',', '.')) . " {$unit_type}";
        }

        /**
         *
         * @param type $length
         * @param type $strength
         * @return string
         */
        public static function generate_random_password($length = 9, $strength = 0) {
            $vowels = 'aeuy';
            $consonants = 'bdghjmnpqrstvz';
            if ($strength & 1) {
                $consonants .= 'BDGHJLMNPQRSTVWXZ';
            }
            if ($strength & 2) {
                $vowels .= "AEUY";
            }
            if ($strength & 4) {
                $consonants .= '23456789';
            }
            if ($strength & 8) {
                $consonants .= '@#$%';
            }
            $fretval = '';
            $alt = time() % 2;
            for ($i = 0; $i < $length; $i++) {
                if ($alt == 1) {
                    $fretval .= $consonants[(rand() % strlen($consonants))];
                    $alt = 0;
                } else {
                    $fretval .= $vowels[(rand() % strlen($vowels))];
                    $alt = 1;
                }
            }
            return $fretval;
        }

        public static function generate_uid($diff, $length = 9) {
            return substr(sha1(uniqid($diff . mt_rand(), true)), 0, $length);
        }

    }

}