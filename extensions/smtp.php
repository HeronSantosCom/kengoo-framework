<?php

if (!class_exists('smtp')) {

    /**
     *
     */
    class smtp {

        private $from, $to, $cc, $bcc, $mail, $attach;
        protected $server, $port, $ssl, $auth, $timeout, $charset;
        public $socket;

        /**
         *
         * @param type $server
         * @param type $port
         * @param type $ssl
         * @param type $auth
         * @param type $timeout
         * @param type $charset
         */
        public function __construct($server, $port = 25, $ssl = false, $auth = false, $timeout = 45, $charset = false) {
            $this->server = ($ssl ? "ssl://{$server}" : $server); // :{$port}
            $this->port = $port;
            $this->host = $server;
            $this->ssl = $ssl;
            $this->auth = $auth;
            $this->timeout = $timeout;
            $this->charset = $charset;
        }

        /**
         *
         * @param type $user
         * @param type $password
         * @return boolean
         */
        public function connect($user = false, $password = false) {
            $this->socket = new socket($this->server, $this->port, $this->timeout);
            if ($this->socket->open()) {
                if ($this->code($this->socket->get()) == '220') {
                    if ($this->auth) {
                        if ($this->code($this->socket->get("EHLO {$this->host}")) == '250') {
                            if ($this->code($this->socket->get("RSET")) == '250') {
                                if ($this->code($this->socket->get("AUTH LOGIN")) == '334') {
                                    if ($this->code($this->socket->get(base64_encode($user))) == '334') {
                                        if ($this->code($this->socket->get(base64_encode($password))) == '235') {
                                            return true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if ($this->code($this->socket->get("HELO {$this->host}")) == '250') {
                        if ($this->code($this->socket->get("RSET")) == '250') {
                            return true;
                        }
                    }
                }
                $this->disconnect();
            }
            return true;
        }

        /**
         *
         * @param type $name
         * @param type $email
         * @return \smtp
         */
        public function from($name, $email) {
            if (!$this->block($email)) {
                $this->from = array($name, $email);
            }
            return $this;
        }

        /**
         *
         * @param type $name
         * @param type $email
         * @return \smtp
         */
        public function to($name, $email) {
            if (!$this->block($email)) {
                $this->to[] = array($name, $email);
            }
            return $this;
        }

        /**
         *
         * @param type $name
         * @param type $email
         * @return \smtp
         */
        public function cc($name, $email) {
            trigger_error('Developer Warning: The function of class CC SMTP is not working properly!');
            if (!$this->block($email)) {
                $this->cc[] = array($name, $email);
            }
            return $this;
        }

        /**
         *
         * @param type $name
         * @param type $email
         * @return \smtp
         */
        public function bcc($name, $email) {
            trigger_error('Developer Warning: The function of class BCC SMTP is not working properly!');
            if (!$this->block($email)) {
                $this->bcc[] = array($name, $email);
            }
            return $this;
        }

        /**
         *
         * @param type $file
         * @param type $description
         * @return \smtp
         */
        public function attach($file, $description) {
            if (file_exists($file)) {
                $pathinfo = pathinfo($file);
                $basename = $pathinfo['basename'];
                $extension = $pathinfo['extension'];
                $mimetype = mimetype($file);
                $this->attach[] = array($file, $mimetype, $basename, $extension, $description);
            }
            return $this;
        }

        /**
         *
         * @param type $subject
         * @param type $content
         * @param type $type
         * @param type $priority
         * @return boolean
         */
        public function mail($subject, $content, $type = "text/plain", $priority = false) {
            if ($this->socket) {
                if ($this->from and $this->to) {
                    $from = "\"{$this->from[0]}\" <{$this->from[1]}>";
                    $cc = false;
                    if ($this->cc) {
                        foreach ($this->cc as $email) {
                            $cc[] = "\"{$email[0]}\" <{$email[1]}>";
                        }
                        $cc = join(", ", $cc);
                    }
                    $bcc = false;
                    if ($this->bcc) {
                        foreach ($this->bcc as $email) {
                            $bcc[] = "\"{$email[0]}\" <{$email[1]}>";
                        }
                        $bcc = join(", ", $bcc);
                    }
                    $response = false;
                    foreach ($this->to as $key => $email) {
                        $response[$key] = false;
                        $to = "\"{$email[0]}\" <{$email[1]}>";
                        if ($this->code($this->socket->get("MAIL FROM: <{$this->from[1]}>")) == '250') {
                            if ($this->code($this->socket->get("RCPT TO: <{$email[1]}>")) == '250') {
                                if ($this->code($this->socket->get("DATA")) == '354') {
                                    $id = date('YmdHis') . '.' . md5(microtime()) . '.' . strtoupper($email[1]);
                                    $boundary = md5(uniqid(time()));
                                    $date = date("r");
                                    $this->socket->put("MIME-Version: 1.0");
                                    $this->socket->put("Reply-To: {$this->from[1]}");
                                    $this->socket->put("Date: {$date}");
                                    $this->socket->put("Delivered-To: {$this->from[1]}");
                                    $this->socket->put("Message-ID: <{$id}>");
                                    $this->socket->put("Subject: {$subject}");
                                    $this->socket->put("From: {$from}");
                                    $this->socket->put("To: {$to}");
                                    if ($cc) {
                                        $this->socket->put("Cc: {$cc}");
                                    }
                                    if ($bcc) {
                                        $this->socket->put("Bcc: {$bcc}");
                                    }
                                    if ($priority) {
                                        $this->socket->put("X-Mailer: " . ips_fullname);
                                        $this->socket->put("X-MSMail-Priority: {$priority}");
                                    }
                                    if ($this->attach) {
                                        $this->socket->put("Content-type: multipart/mixed; boundary={$boundary}");
                                        $this->socket->put("\r\n");
                                        $this->socket->put("--" . $boundary);
                                    }
                                    $this->socket->put("Content-Type: {$type}; charset={$this->charset}");
//                                $this->socket->put('Content-Transfer-Encoding: 8bit');
                                    $this->socket->put("\r\n");
                                    $this->socket->put($content);
                                    //$this->socket->put("\r\n");
                                    if ($this->attach) {
                                        $this->socket->put("\r\n");
                                        foreach ($this->attach as $file) {
                                            $id = date('YmdHis') . '.' . md5(microtime()) . '.' . strtoupper($email[1]);
                                            $this->socket->put("--{$boundary}");
                                            $this->socket->put("Content-Type: {$file[1]}; name=\"{$file[2]}\"");
                                            $this->socket->put("Content-Disposition: attachment; filename=\"{$file[2]}\"");
                                            $this->socket->put("Content-Transfer-Encoding: base64");
                                            $this->socket->put("X-Attachment-Id: {$id}");
                                            $this->socket->put("\r\n");
                                            $this->socket->put(chunk_split(base64_encode(file_get_contents($file[0]))));
                                            $this->socket->put('--' . $boundary . '--');
                                            //$this->socket->put("\r\n");
                                        }
                                    }
                                    //if ($this->code($this->socket->get("\r\n\r\n.")) == '250') {
                                    if ($this->code($this->socket->get(".")) == '250') {
                                        $response[$key] = true;
                                    }
                                }
                            }
                        }
                    }
                    return $response;
                }
            }
            return false;
        }

        /**
         *
         * @return boolean
         */
        public function disconnect() {
            if ($this->socket) {
                if ($this->socket->close()) {
                    $this->socket = false;
                    return true;
                }
            }
            return true;
        }

        /**
         *
         * @param type $value
         * @param type $length
         * @return boolean
         */
        private function code($value, $length = 3) {
            if (is_array($value)) {
                $value = join("\n", $value);
            }
            if (strlen($value) > ($length - 1)) {
                return substr(trim($value), 0, $length);
            }
            return false;
        }

        /**
         *
         * @param type $email
         * @return boolean
         */
        private function block($email) {
            if (preg_match("#(@127.0.0.1|@localhost|@localdomain)#", $email)) {
                return true;
            }
            return false;
        }

    }

}