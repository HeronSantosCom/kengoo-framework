<?php

if (!class_exists('object')) {

    /**
     *
     */
    class object
    {
        private $array = array();

        public function __construct($array = false)
        {
            $this->array = $this->inject($array);
        }

        public function __set($name, $value)
        {
            return $this->array[$name] = $value;
        }

        public function __get($name)
        {
            return (!empty($this->array[$name]) ? $this->array[$name] : false);
        }

        public function is_empty()
        {
            return (count($this->array) > 0 ? false : true);
        }

        public function extract() {
            return $this->array;
        }

        public function merge($arrays) {
            $args = func_get_args();
            if ($args) {
                foreach($args as $key => $values) {
                    if ($this->is_empty()) {
                        $this->array = $values;
                    } else {
                        $this->array = array_merge($this->array, $values);
                    }
                }
            }
        }

        private function inject($array) {
            $obj = array();
            if (is_array($array)) {
                $size = sizeof($array);
                $keys = array_keys($array);
                for ($x = 0; $x < $size; $x++) {
                    $key = $keys[$x];
                    $value = $array[$key];
                    // ----
                    $key = (is_string($key) ? $key : "i{$key}");
                    $obj[$key] = (is_array($value) ? $this->extract($value) : $value);
                    // ----
                    unset($key, $value);
                }
                unset($size, $keys, $x);
            }
            return $obj;
        }
    }

}