<?php

if (!class_exists('mysqldelete')) {

    /**
     * Esta versão será substituida pela versão única do mysql a ser construída.
     * Contará com algumas melhorias e implementações que constam nesta classe.
     */
    class mysqldelete extends mysqlwhere {

        /**
         *
         */
        public function __construct() {
            parent::__construct();
            $this->method = "delete";
        }

        /**
         *
         * @param type $name
         * @return \mysqldelete
         */
        public function database($name) {
            $index = $this->index("database");
            $this->database[$index] = $this->ereaser($name, "`");
            return $this;
        }

        /**
         *
         * @param type $name
         * @param type $databasekey
         * @return \mysqldelete
         */
        public function table($name, $databasekey = 1) {
            $alias = false;
            $database = (isset($this->database[$databasekey]) ? "{$this->database[$databasekey]}." : $this->ereaser($this->select, "`") . ".");
            $table = $this->ereaser($name, "`");
            $index = $this->index("table");
            $alias = $database . $table;
            $this->table[$index] = array(false, $alias, $alias);
            return $this;
        }

        /**
         *
         * @return boolean
         */
        public function go() {
            $table = $where = null;
            if ($this->table) {
                foreach ($this->table as $value) {
                    $table .= ( $value[0] ? " " : (is_null($table) ? null : ", ")) . $value[1];
                }
                if ($this->where) {
                    $where = " WHERE " . join(" ", $this->where);
                }
                $query = "DELETE FROM {$table}{$where}";
                $this->result = $this->commit($query);
                if ($this->result["result"]) {
                    if ($this->result["rows"] > 0) {
                        return true;
                    }
                }
                return false;
            }
            trigger_error("Table not defined!", E_USER_NOTICE);
            return false;
        }

    }

}
