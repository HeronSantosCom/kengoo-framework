<?php

if (!class_exists('communicator')) {

    /**
     *
     */
    class communicator extends app {

        /**
         *
         * @return type
         */
        public function __construct() {
            if ($_SERVER["HTTP_USER_AGENT"] == sys) {
                extract($_POST);
                if (!empty($app) and !empty($args)) {
                    $app = self::dec($app);
                    $args = self::dec($args);
                    ob_start();
                    $instance = "cfc-" . md5($app);
                    $class = explode(".", trim(str_replace(array("-", "/", ">"), " ", $app)));
                    if (isset($class[1])) {
                        $static = $class[1];
                        $class = $class[0];
                        if (!isset($this->$instance)) {
                            $this->$instance = new $class();
                            if (knife::get_class_const($class, 'certificate') != certificate) {
                                trigger_error("The required application is not compatible with the platform...");
                                return die();
                            }
                        }
                        die(self::enc($this->$instance->$static($args)));
                    } else {
                        $class = $class[0];
                        if (!isset($this->$instance)) {
                            $this->$instance = new $class();
                            if (knife::get_class_const($class, 'certificate') != certificate) {
                                trigger_error("The required application is not compatible with the platform...");
                                return die();
                            }
                        }
                        die(self::enc($this->$instance->$class($args)));
                    }
                }
            }
            die(false);
        }

        /**
         *
         * @param type $val
         * @return type
         */
        private static function enc($val) {
            $md5 = new md5();
            return $md5->set($val);
        }

        /**
         *
         * @param type $val
         * @return type
         */
        private static function dec($val) {
            $md5 = new md5();
            return $md5->get($val);
        }

        /**
         *
         * @param type $url
         * @param type $app
         * @param type $args
         * @return type
         */
        public static function get($url, $app, $args = false) {
            $app = urlencode(self::enc($app));
            $args = urlencode(self::enc($args));
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_TIMEOUT, 15);
            curl_setopt($ch, CURLOPT_USERAGENT, sys);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "app={$app}&args={$args}");
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            ob_start();
            curl_exec($ch);
            curl_close($ch);
            $data = ob_get_contents();
            ob_end_clean();
            if (preg_match('/301 Moved Permanently/i', $data)) {
                preg_match("/HREF=\"(.*)\"/si", $data, $r);
                return self::get($r[1], $app, $args);
            }
            return self::dec($data);
        }

    }

}