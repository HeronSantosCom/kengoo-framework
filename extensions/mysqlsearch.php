<?php

if (!class_exists('mysqlsearch')) {

    /**
     * Esta versão será substituida pela versão única do mysql a ser construída.
     * Contará com algumas melhorias e implementações que constam nesta classe.
     */
    class mysqlsearch extends mysqlwhere {

        /**
         *
         */
        public function __construct() {
            parent::__construct();
            $this->method = "select";
        }

        /**
         *
         * @param type $name
         * @return \mysqlsearch
         */
        public function database($name) {
            $index = $this->index("database");
            $this->database[$index] = $this->ereaser($name, "`");
            return $this;
        }

        /**
         *
         * @param type $name
         * @param type $databasekey
         * @param type $alias
         * @return \mysqlsearch
         */
        public function table($name, $databasekey = 1, $alias = false) {
            $database = (isset($this->database[$databasekey]) ? "{$this->database[$databasekey]}." : $this->ereaser($this->select, "`") . ".");
            $table = $this->ereaser($name, "`");
            $index = $this->index("table");
            $alias = ($alias ? $alias : $this->ereaser("t{$index}", "`"));
            $this->table[$index] = array(false, "{$database}{$table} {$alias}", $alias, "{$database}{$table}", $table);
            return $this;
        }

        /**
         *
         * @param type $name
         * @param array $condition ("coluna da tabela 1", "condição", "coluna da tabela 2", "tabela 1", "tabela 2")
         * @param type $factor
         * @param type $databasekey
         * @param type $alias
         * @return boolean|\mysqlsearch
         */
        public function join($name, $condition = false, $factor = false, $databasekey = 1, $alias = false) {
            $index_condition = $this->index("table", true);
            $database = (isset($this->database[$databasekey]) ? "{$this->database[$databasekey]}." : $this->ereaser($this->select, "`") . ".");
            $table = $this->ereaser($name, "`");
            $index = $this->index("table");
            $alias = ($alias ? $alias : $this->ereaser("t{$index}", "`"));

            switch ($factor) {
                case "LEFT":
                    $join = "LEFT JOIN";
                    break;
                case "RIGHT":
                    $join = "RIGHT JOIN";
                    break;
                case "INNER":
                    $join = "INNER JOIN";
                    break;
                default:
                    if (!$factor) {
                        $join = "JOIN";
                    } else {
                        trigger_error("Factor to do a JOIN in your query is not allowed!");
                        return false;
                    }
                    break;
            }

            $on = null;
            if ($condition) {
                if (is_array($condition)) {
                    if (!isset($this->table[$index_condition])) {
                        trigger_error("Table for comparison previously unreported!");
                        return false;
                    }
                    $column_condition_1 = $this->ereaser($condition[0], "`");
                    $column_condition_2 = (isset($condition[2]) ? $this->ereaser($condition[2], "`") : $column_condition_1);
                    $table_condition_1 = $this->table[(isset($condition[3]) ? $condition[3] : $index_condition)][2] . ".{$column_condition_1}";
                    $table_condition_2 = (isset($condition[4]) ? $this->table[$condition[4]][2] : $alias) . ".{$column_condition_2}";
                    //$table_condition_2 = "{$alias}.{$column_condition_2}";
                    $operator = (isset($condition[1]) ? trim($condition[1]) : "=");
                    $condition = "{$table_condition_1} {$operator} {$table_condition_2}";
                }
                $on = " ON ({$condition})";
            }

            $this->table[$index] = array(true, "{$join} {$database}{$table} {$alias}{$on}", $alias, "{$database}{$table}", $table);
            return $this;
        }

        /**
         *
         * @param type $name
         * @param type $tablekey
         * @param type $alias
         * @return boolean|\mysqlsearch
         */
        public function column($name, $tablekey = 1, $alias = false) {
            if (!($name == "*" && $tablekey == false)) {
                $table = null;
                if (!$this->is_function($alias)) {
                    $index = $this->index("column");
                    if (!$this->is_function($name)) {
                        if (!isset($this->table[$tablekey])) {
                            trigger_error("Requested table unreported!");
                            return false;
                        }
                        $column = $this->ereaser($name);
                        if ($name != "*") {
                            //$alias = $column;
                            $table = "{$this->table[$tablekey][2]}.";
                            $column = $table . $this->ereaser($column, "`", false);
                        } else {
                            $column = "{$this->table[$tablekey][2]}.{$column}";
                        }
                    } else {
                        if ($tablekey) {
                            trigger_error("Unable to determine table in a function!");
                            return false;
                        } else {
                            if (!$alias) {
                                trigger_error("Alias ​​for function not determined!");
                                return false;
                            }
                        }
                        $column = $name;
                        $alias = $this->ereaser($alias);
                    }
                    $this->columns[$index] = array($column . ($alias ? " AS '{$alias}'" : null), $alias);
                    return $this;
                }
                trigger_error("Alias entered is not allowed!");
                return false;
            }
            $tables = $this->table;
            if ($tables) {
                foreach ($tables as $k => $table) {
                    $db = new mysqlshow();
                    $columns = $db->columns($table[3]);
                    if ($columns) {
                        $table_alias = $this->ereaser($table[4]);
                        foreach ($columns as $j => $column) {
                            $alias = ($table[0] ? "{$table_alias}_{$column["Field"]}" : false);
                            $index = $this->index("column");
                            $this->columns[$index] = array("{$this->table[$k][2]}." . $this->ereaser($column["Field"], "`", false) . ($alias ? " AS '{$alias}'" : null), $alias);
                        }
                    }
                }
            }
            return $this;
        }

        /**
         *
         * @param type $columnkey
         * @return boolean|\mysqlsearch
         */
        public function group($column) {
            if (!isset($this->columns[$column])) {
                $column = $this->ereaser($column, "`");
            } else {
                $column = ($this->columns[$column][1] ? $this->ereaser($this->columns[$column][1], "`") : $this->columns[$column][0]);
            }
            $index = $this->index("group");
            $this->group[$index] = $column;
            return $this;
        }

        /**
         *
         * @param type $columnkey
         * @param type $value
         * @param type $operator
         * @return boolean|\mysqlsearch
         */
        public function having($column, $value, $operator = "=") {
//            if (!$this->is_function($column)) {
            if (!isset($this->columns[$column])) {
                $column = $this->ereaser($column, "`");
            } else {
                $column = ($this->columns[$column][1] ? $this->ereaser($this->columns[$column][1], "`") : $this->columns[$column][0]);
            }
//            }
            $value = trim($value);
            if (!$this->is_function($value)) {
                $value = (strlen($value) > 0 ? "\"" . mysqli_real_escape_string($this->link, $value) . "\"" : "NULL");
            }
            $index = $this->index("having");
            $this->having[$index] = "{$column} {$operator} {$value}";
            return $this;
        }

        /**
         *
         * @param type $column
         * @param type $order
         * @return \mysqlsearch
         */
        public function order($column, $order = "ASC") {
            if (!isset($this->columns[$column])) {
                $column = $this->ereaser($column, "`");
            } else {
                $column = ($this->columns[$column][1] ? $this->ereaser($this->columns[$column][1], "`") : $this->columns[$column][0]);
            }
            $index = $this->index("order");
            $this->order[$index] = $column . strtoupper(" {$order}");
            return $this;
        }

        /**
         *
         * @param type $limit
         * @param type $length
         * @return \mysqlsearch
         */
        public function limit($limit, $length = false) {
            $this->limit = $limit . ($length ? ", {$length}" : null);
            return $this;
        }

        /**
         *
         * @param type $fetch_object
         * @return boolean
         */
        public function go($fetch_object = false, $summary = true) {
            $table = $columns = $where = $group = $having = $order = $limit = null;
            if ($this->table) {
                foreach ($this->table as $value) {
                    $table .= ( $value[0] ? " " : (is_null($table) ? null : ", ")) . $value[1];
                }
                if ($this->columns) {
                    foreach ($this->columns as $value) {
                        $columns .= ( is_null($columns) ? null : ", ") . $value[0];
                    }
                    if ($this->where) {
                        $where = " WHERE " . join(" ", $this->where);
                    }
                    if ($this->group) {
                        $group = " GROUP BY " . join(", ", $this->group);
                    }
                    if ($this->having) {
                        $having = " HAVING " . join(" AND ", $this->having);
                    }
                    if ($this->order) {
                        $order = " ORDER BY " . join(", ", $this->order);
                    }
                    if ($this->limit) {
                        $limit = " LIMIT " . $this->limit;
                    }
                    $query = "SELECT SQL_CALC_FOUND_ROWS {$columns} FROM {$table}{$where}{$group}{$having}{$order}{$limit}";
                    $this->result = $this->commit($query);
                    if ($this->result["result"]) {
                        $search = false;
                        if (@mysqli_num_rows($this->result["result"]) > 0) {
                            if ($fetch_object) {
                                while ($line = mysqli_fetch_object($this->result["result"])) {
                                    $search[] = $line;
                                }
                            } else {
                                while ($line = mysqli_fetch_assoc($this->result["result"])) {
                                    $search[] = $line;
                                }
                            }
                            mysqli_free_result($this->result["result"]);
                        }
                        return ($summary ? array($search, $this->rows()) : $search);
                    }
                    return false;
                }
                trigger_error("Column unreported!");
                return false;
            }
            trigger_error("Table unreported!");
            return false;
        }

    }

}