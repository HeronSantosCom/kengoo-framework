<?php

if (!class_exists('mysqlsave')) {

    /**
     * Esta versão será substituida pela versão única do mysql a ser construída.
     * Contará com algumas melhorias e implementações que constam nesta classe.
     */
    class mysqlsave extends mysqlwhere {

        /**
         *
         */
        public function __construct() {
            parent::__construct();
            $this->method = "save";
        }

        /**
         *
         * @param type $name
         * @return \mysqlsave
         */
        public function database($name) {
            $index = $this->index("database");
            $this->database[$index] = $this->ereaser($name, "`");
            return $this;
        }

        /**
         *
         * @param type $name
         * @param type $databasekey
         * @return \mysqlsave
         */
        public function table($name, $databasekey = 1) {
            $database = (isset($this->database[$databasekey]) ? "{$this->database[$databasekey]}." : $this->ereaser($this->select, "`") . ".");
            $table = $this->ereaser($name, "`");
            $index = $this->index("table");
            $alias = $database . $table;
            $this->table[$index] = array(false, $alias, $alias);
            return $this;
        }

        /**
         *
         * @param type $name
         * @param type $value
         * @param type $tablekey
         * @return \mysqlsave
         */
        public function column($name, $value = null, $tablekey = false) {
            $table = (isset($this->table[0]) ? "{$this->table[0]}." : null);
            if ($tablekey) {
                $table = (isset($this->table[$tablekey]) ? "{$this->table[$tablekey]}." : $table);
            }
            $value = trim($value);
            $column = $this->ereaser($name, "`");
            if (!$this->is_function($value)) {
                $value = (strlen($value) > 0 ? "\"" . mysqli_real_escape_string($this->link, $value) . "\"" : "NULL");
            } else {
                $value = (strlen($value) > 0 ? mysqli_real_escape_string($this->link, $value) : "NULL");
            }
            $this->columns[] = $table . $column . " = " . $value;
            return $this;
        }

        /**
         *
         * @param string $ignore
         * @return boolean
         */
        public function go($ignore = false) {
            if ($ignore) {
                $ignore = "IGNORE";
            }
            $table = null;
            if ($this->table) {
                foreach ($this->table as $value) {
                    $table .= ( $value[0] ? " " : (is_null($table) ? null : ", ")) . $value[1];
                }
                if ($this->columns) {
                    $columns = join(", ", $this->columns);
                    $query = "INSERT {$ignore} INTO {$table} SET {$columns}";
                    if ($this->where) {
                        $where = join(" ", $this->where);
                        $query = "UPDATE {$ignore} {$table} SET {$columns} WHERE {$where}";
                    }
                    $this->result = $this->Commit($query);
                    if ($this->result["result"]) {
                        return true;
                    }
                    return false;
                }
                trigger_error("Column / values ​​not defined!");
                return false;
            }
            trigger_error("Table not defined!");
            return false;
        }

        /**
         *
         * @return boolean
         */
        public function id() {
            if (isset($this->result["id"])) {
                return $this->result["id"];
            }
            return false;
        }

    }

}
