<?php

if (!class_exists('uri')) {

    /**
     *
     */
    class uri {

        /**
         *
         * @param type $filename
         * @return type
         */
        static function rtrim($filename) {
            if (substr($filename, (strlen($filename) - 1)) == "/") {
                $filename = substr($filename, 0, (strlen($filename) - 1));
            }
            return $filename;
        }

        /**
         *
         * @param type $filename
         * @return type
         */
        static function ltrim($filename) {
            if (substr($filename, 0, 1) == "/") {
                $filename = substr($filename, 1);
            }
            return $filename;
        }

        /**
         *
         * @param type $filename
         * @return boolean
         */
        static function concat($filename) {
            if (strlen($filename) > 0) {
                return "/{$filename}";
            }
            return false;
        }

        /**
         *
         * @param type $filename
         * @return type
         */
        static function trim($filename) {
            if (strlen($filename) > 1) {
                if (substr($filename, (strlen($filename) - 1)) == "/") {
                    $filename = substr($filename, 0, (strlen($filename) - 1));
                }
            }
            return $filename;
        }

        /**
         *
         * @param type $filename
         * @return type
         */
        static function root($filename = false) {
            $root = explode("/", $_SERVER["SCRIPT_NAME"]);
            $root = (is_array($root) ? array_slice($root, 0, (count($root) - 1)) : false);
            $root = (is_array($root) ? implode("/", $root) : false);
            return self::trim($root . self::concat($filename));
        }

        /**
         *
         * @param type $filename
         * @return type
         */
        static function dir($filename = false) {
            $filename = ($filename ? $filename : self::get());
            $folder = explode("/", $filename);
            if (count($folder) > 0) {
                if (mimetype($folder[(count($folder) - 1)])) {
                    $folder = array_slice($folder, 0, (count($folder) - 1));
                }
            }
            return self::rtrim(implode("/", $folder));
        }

        /**
         *
         * @param type $filename
         * @return boolean
         */
        static function args($filename = false) {
            $filename = ($filename ? $filename : $_SERVER["REQUEST_URI"]);
            $args = explode("?", $_SERVER["REQUEST_URI"], 2);
            if (isset($args[1])) {
                $args = explode("#", $args[1], 2);
                $args = urldecode($args[0]);
                return "?" . $args;
            }
            return false;
        }

        /**
         *
         * @param type $filename
         * @return type
         */
        static function get($filename = false) {
            $uri = $_SERVER["REQUEST_URI"];
            $uri = explode("?", $_SERVER["REQUEST_URI"], 2);
            $uri = explode("#", $uri[0], 2);
            $uri = urldecode($uri[0]);
            $root = self::root();
            if (strlen($root) > 0) {
                $uri = substr($uri, strlen($root));
            }
            return self::trim($uri . self::concat($filename));
        }

    }

}