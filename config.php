<?php

if (!class_exists('config')) {

    /**
     *
     */
    class config {

        /**
         *
         * @param type $content
         * @param type $node
         */
        public static function export($content, $node = false) {
            if ($content) {
                foreach ($content as $key => $value) {
                    $new = ($node ? $node . "_" . $key : $key);
                    $string = trim((string) $value);
//                    knife::dump(gettype($string));
//                    if (strlen($string) > 0) {
                        if (!defined($new)) {
                            define($new, self::logic($string));
                        }
//                    } else {
                        if (!is_null($key)) {
                            self::export($value, $new);
                        }
//                    }
                }
            }
        }

        /**
         *
         * @param type $string
         * @return boolean|null
         */
        private static function logic($string) {
            switch ($string) {
                case "true":
                    return true;
                    break;
                case "false":
                    return false;
                    break;
                case "null":
                    return null;
                    break;
            }
            return $string;
        }

    }

}