<?php

if (!class_exists('app')) {

    /**
     *
     */
    class app {

        const certificate = certificate;

        /**
         *
         * @param type $name
         * @param type $value
         * @return type
         */
        public function __set($name, $value) {
            return $_ENV[certificate][$name] = $value;
        }

        /**
         *
         * @param type $name
         * @return boolean
         */
        public function __get($name) {
            if (isset($this->$name)) {
                return $_ENV[certificate][$name];
            }
            return false;
        }

        /**
         *
         * @param type $name
         * @return type
         */
        public function __isset($name) {
            return isset($_ENV[certificate][$name]);
        }

        /**
         *
         * @param type $name
         */
        public function __unset($name) {
            unset($_ENV[certificate][$name]);
        }

        /**
         *
         * @param type $array
         * @param type $prefix
         */
        public function extract($array, $prefix = false, $recursive = true) {
            if (is_array($array)) {
                foreach ($array as $key => $value) {
                    if (is_array($value) && $recursive) {
                        $this->extract($value, ($prefix ? "{$prefix}_{$key}" : $key));
                    } else {
                        $var = ($prefix ? "{$prefix}_{$key}" : $key);
                        $this->$var = $value;
                    }
                }
            }
        }

    }

}