<?php

if (!class_exists('install')) {

    /**
     *
     */
    class install {

        /**
         *
         */
        public function __construct() {
            if (!$this->config()) {
                trigger_error("Não foi possível criar/recriar arquivo de configuração!", E_USER_ERROR);
            }
            if (!$this->applications()) {
                trigger_error("Não foi possível criar/recriar pasta de aplicações!", E_USER_ERROR);
            }
            if (!$this->sources()) {
                trigger_error("Não foi possível criar/recriar pasta de código-fonte!", E_USER_ERROR);
            }
            if (!$this->logs()) {
                trigger_error("Não foi possível criar/recriar pasta de eventos!", E_USER_ERROR);
            }
            if (!$this->main()) {
                trigger_error("Não foi possível criar/recriar arquivo exemplo de aplicação!", E_USER_ERROR);
            }
            if (!$this->service()) {
                trigger_error("Não foi possível criar/recriar arquivo de serviço!", E_USER_ERROR);
            }
            if (!$this->index()) {
                trigger_error("Não foi possível criar/recriar arquivo de inicialização!", E_USER_ERROR);
            }
            if (!$this->favicon()) {
                trigger_error("Não foi possível criar/recriar icone de favorito!", E_USER_ERROR);
            }
            if (!$this->robots()) {
                trigger_error("Não foi possível criar/recriar controlador de robôs de pesquisas!", E_USER_ERROR);
            }
            if (!$this->htaccess()) {
                trigger_error("Não foi possível criar/recriar arquivo de configuração de acesso!", E_USER_ERROR);
            }
//        if (!$this->tail()) {
//            trigger_error("Não foi possível criar/recriar arquivo de monitoramento!", E_USER_ERROR);
//        }
        }

        /**
         *
         * @return boolean
         */
        private function config() {
            $config = configurations;
            if (!is_file($config)) {
                $content[] = '<?xml version="1.0" encoding="UTF-8"?>';
                $content[] = '<root>';
                $content[] = "    " . '<config status="1" permalink="0" maintenance="0">';
                $content[] = "        " . '<![CDATA[Novo Site: ' . domain . ']]>';
                $content[] = "    " . '</config>';
                $content[] = "    " . '<information>';
                $content[] = "        " . '<domain><![CDATA[' . domain . ']]></domain>';
                $content[] = "        " . '<date><![CDATA[' . date("r") . ']]></date>';
                $content[] = "    " . '</information>';
                $content[] = "    " . '<defines>';
                $content[] = "    " . '<mysql>';
                $content[] = "            " . '<server>localhost</server>';
                $content[] = "            " . '<database>test</database>';
                $content[] = "            " . '<username>root</username>';
                $content[] = "            " . '<password>root</password>';
                $content[] = "            " . '<limit>3000</limit>';
                $content[] = "            " . '<global>true</global>';
                $content[] = "            " . '<!--<dump>true</dump>-->';
                $content[] = "    " . '</mysql>';
                $content[] = "        " . '<encoding>';
                $content[] = "            " . '<application>utf8</application>';
                $content[] = "            " . '<source>utf-8</source>';
                $content[] = "        " . '</encoding>';
                $content[] = "        " . '<timezone>';
                $content[] = "            " . '<locale>pt_BR</locale>';
                $content[] = "            " . '<unix>America/Sao_Paulo</unix>';
                $content[] = "            " . '<gmt>-3:00</gmt>';
                $content[] = "        " . '</timezone>';
                $content[] = "    " . '</defines>';
                $content[] = '</root>';
                return file_put_contents($config, join("\n", $content));
            }
            return true;
        }

        /**
         *
         * @return boolean
         */
        private function applications() {
            if (!is_dir(applications)) {
                if (mkdir(applications)) {
                    return true;
                }
                return false;
            }
            return true;
        }

        /**
         *
         * @return boolean
         */
        private function sources() {
            if (!is_dir(sources)) {
                if (mkdir(sources)) {
                    return true;
                }
                return false;
            }
            return true;
        }

        /**
         *
         * @return boolean
         */
        private function logs() {
            if (!is_dir(logs)) {
                if (mkdir(logs)) {
                    return true;
                }
                return false;
            }
            return true;
        }

        /**
         *
         * @return boolean
         */
        private function tail() {
            /*
              $tail = path::sources() . "/log.php";
              if (!is_file($tail)) {
              $content[] = '<?php';
              $content[] = "    " . 'if (!class_exists("phptail", false)) {';
              $content[] = "        " . 'include path::plugins("phptail.php");';
              $content[] = "    " . '}';
              $content[] = "    " . 'new phptail(path::logs() . path::concat(date("Ymd") . ".log"));';
              $content[] = '?>';
              return file_put_contents($tail, join("\n", $content));
              }
             */
            return true;
        }

        /**
         *
         * @return boolean
         */
        private function main() {
            $index = path::applications() . "/main.php";
            if (!is_file($index)) {
                return copy(path::template("main.php"), $index);
            }
            return true;
        }
        
        /**
         * 
         * @return boolean
         */
        private function service() {
            $index = service;
            if (!is_file($index)) {
                return copy(path::template("service.php"), $index);
            }
            return true;
        }

        /**
         *
         * @return boolean
         */
        private function index() {
            $index = path::sources() . "/index.html";
            if (!is_file($index)) {
                return copy(path::template("newproject.html"), $index);
            }
            return true;
        }

        /**
         *
         * @return boolean
         */
        private function favicon() {
            $index = path::sources() . "/favicon.ico";
            if (!is_file($index)) {
                return copy(path::template("favicon.ico"), $index);
            }
            return true;
        }

        /**
         *
         * @return boolean
         */
        private function robots() {
            $index = path::sources() . "/robots.txt";
            if (!is_file($index)) {
                return copy(path::template("robots.txt"), $index);
            }
            return true;
        }
        
        /**
         * 
         * @return boolean
         */
        private function htaccess() {
            $index = htaccess;
            if (!is_file($index)) {
                return copy(path::template(".htaccess"), $index);
            }
            return true;
        }

    }

}