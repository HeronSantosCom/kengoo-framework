<?php

class pagseguro {

    const api_email = 'heronsantos@outlook.com';
    const api_key = '7DD304A64C1E4E80B6784D7AB6B4A245';

    private $retorno = 'http://painel.insigndigital.com.br/?saldo';
    private $pagseguro, $itens, $referencia, $nome, $email = false;

    public function __construct($referencia, $nome, $email) {
        $this->referencia = $referencia;
        $this->nome = $nome;
        $this->email = $email;
    }

    public function item($descricao, $valor, $quantidade = 1) {
        $item = array($descricao, $valor, $quantidade);
        if (empty($this->itens)) {
            return $this->itens = array($item);
        }
        return $this->itens[] = $item;
    }

    public function go() {
        $this->pagseguro = curl_init();
        if ($this->pagseguro && $this->itens) {

            $post = array(
                "email" => self::api_email,
                "token" => self::api_key,
                "currency" => "BRL",
                "reference" => $this->referencia,
                "senderName" => $this->nome,
                "senderEmail" => $this->email,
                "redirectURL" => $this->retorno
            );

            $x = 1;
            foreach ($this->itens as $item) {
                $post["itemId{$x}"] = $x;
                $post["itemDescription{$x}"] = $item[0];
                $post["itemAmount{$x}"] = $item[1];
                $post["itemQuantity{$x}"] = $item[2];
                $x++;
            }

            foreach ($post as $key => $value) {
                $post[$key] = "{$key}={$value}";
            }

            curl_setopt($this->pagseguro, CURLOPT_URL, 'https://ws.pagseguro.uol.com.br/v2/checkout');
            curl_setopt($this->pagseguro, CURLOPT_POST, 1);
            curl_setopt($this->pagseguro, CURLOPT_HTTPHEADER, array('Content-type: application/x-www-form-urlencoded;charset=UTF-8'));
            curl_setopt($this->pagseguro, CURLOPT_POSTFIELDS, join("&", $post));
            curl_setopt($this->pagseguro, CURLOPT_COOKIEJAR, 'cookie.txt');
            curl_setopt($this->pagseguro, CURLOPT_RETURNTRANSFER, 1);
            $resposta = curl_exec($this->pagseguro);
            if (!($resposta === false)) {
                $xml = @simplexml_load_string($resposta);
                if (!empty($xml->code)) {
                    curl_close($this->pagseguro);
                    knife::redirect("https://pagseguro.uol.com.br/v2/checkout/payment.html?code={$xml->code}");
                    return true;
                }
            }
        }
        return false;
    }

    public static function notificacao($referencia) {

        $post = array(
            "email" => self::api_email,
            "token" => self::api_key
        );

        foreach ($post as $key => $value) {
            $post[$key] = "{$key}=" . urlencode($value);
        }

        $resposta = knife::open("https://ws.pagseguro.uol.com.br/v2/transactions/notifications/{$referencia}?" . join("&", $post));
        if (!($resposta === false)) {
            $xml = @simplexml_load_string($resposta);
            if ($xml) {
                return $xml;
            }
        }

        return false;
    }

}
