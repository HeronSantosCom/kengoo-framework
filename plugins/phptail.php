<?php

class phptail {

    /**
     * Location of the log file we're tailing
     * @var string
     */
    private $log = "";

    /**
     * The time between AJAX requests to the server.
     *
     * Setting this value too high with an extremly fast-filling log will cause your PHP application to hang.
     * @var integer
     */
    private $updateTime;

    /**
     * This variable holds the maximum amount of bytes this application can load into memory (in bytes).
     * @var string
     */
    private $maxSizeToLoad;

    /**
     *
     * PHPTail constructor
     * @param string $log the location of the log file
     * @param integer $defaultUpdateTime The time between AJAX requests to the server.
     * @param integer $maxSizeToLoad This variable holds the maximum amount of bytes this application can load into memory (in bytes). Default is 2 Megabyte = 2097152 byte
     */
    public function __construct($log, $defaultUpdateTime = 2000, $maxSizeToLoad = 2097152) {
        $this->log = $log;
        $this->updateTime = $defaultUpdateTime;
        $this->maxSizeToLoad = $maxSizeToLoad;

        /**
         * We're getting an AJAX call
         */
        if (isset($_GET['ajax'])) {
            echo $this->getNewLines($_GET['lastsize'], $_GET['grep'], $_GET['invert']);
            die();
        }
        /**
         * Regular GET/POST call, print out the GUI
         */
        $this->generateGUI();
    }

    /**
     * This function is in charge of retrieving the latest lines from the log file
     * @param string $lastFetchedSize The size of the file when we lasted tailed it.
     * @param string $open_settings The grep keyword. This will only return rows that contain this word
     * @return Returns the JSON representation of the latest file size and appended lines.
     */
    public function getNewLines($lastFetchedSize, $open_settings, $invert) {

        /**
         * Clear the stat cache to get the latest results
         */
        clearstatcache();
        /**
         * Define how much we should load from the log file
         * @var
         */
        $fsize = filesize($this->log);
        $maxLength = ($fsize - $lastFetchedSize);
        /**
         * Verify that we don't load more data then allowed.
         */
        if ($maxLength > $this->maxSizeToLoad) {
            return json_encode(array("size" => $fsize, "data" => array("ERROR: PHPTail attempted to load more (" . round(($maxLength / 1048576), 2) . "MB) then the maximum size (" . round(($this->maxSizeToLoad / 1048576), 2) . "MB) of bytes into memory. You should lower the defaultUpdateTime to prevent this from happening. ")));
        }
        /**
         * Actually load the data
         */
        $data = array();
        if ($maxLength > 0) {

            $fp = fopen($this->log, 'r');
            fseek($fp, -$maxLength, SEEK_END);
            $data = explode("\n", htmlentities(fread($fp, $maxLength)));
        }
        /**
         * Run the grep function to return only the lines we're interested in.
         */
        if ($invert == 0) {
            $data = preg_grep("/$open_settings/", $data);
        } else {
            $data = preg_grep("/$open_settings/", $data, PREG_GREP_INVERT);
        }
        /**
         * If the last entry in the array is an empty string lets remove it.
         */
        if (end($data) == "") {
            array_pop($data);
        }
        return json_encode(array("size" => $fsize, "data" => $data));
    }

    /**
     * This function will print out the required HTML/CSS/JS
     */
    public function generateGUI() {
        ?>
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <title>Visualizador de Eventos</title>
                <meta http-equiv="content-type" content="text/html;charset=utf-8" />
                <link type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/themes/flick/jquery-ui.css" rel="stylesheet"></link>
                <style type="text/css">
                    * {
                        margin: 0;
                        padding: 0;
                    }
                    
                    html {
                        height: 100%;
                    }
                    
                    body {
                        width: 100%;
                        height: 100%;
                        background: black;
                        overflow-x:hidden;
                    }
                    
                    .float {
                        background: white;
                        color: black;
                        position: absolute;
                        right: 0;
                        top: 0px;
                        border-bottom: 1px solid black;
                        padding: 10px;
                    }
                    
                    pre {
                        color: white;
                        font-family: monospace;
                        font-size: 10pt;
                    }

                    #settings p {
                        padding: 10px 0;
                    }
                </style>
                <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>
                <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.9/jquery-ui.min.js"></script>
                <script type="text/javascript">
                    var lastSize = <?php echo filesize($this->log); ?>;
                    var grep = "";
                    var invert = 0;
                    var documentHeight = 0;
                    var scrollPosition = 0;
                    var scroll = true;
                    function scrollToBottom() {
                        $('.ui-widget-overlay').width($(document).width());
                        $('.ui-widget-overlay').height($(document).height());
                        $("html, body").scrollTop($(document).height());
                        if($( "#settings").dialog("isOpen")) {
                            $('.ui-widget-overlay').width($(document).width());
                            $('.ui-widget-overlay').height($(document).height());
                            $( "#settings" ).dialog("option", "position", "center");
                        }
                    }
                    function updateLog() {
                        $.getJSON('?ajax=1&lastsize='+lastSize + '&grep='+grep + '&invert='+invert, function(data) {
                            lastSize = data.size;
                            $.each(data.data, function(key, value) {
                                $("#results").append(value + "\n");
                            });
                            if(scroll) {
                                scrollToBottom();
                            }
                        });
                    }
                    $(document).ready(function(){
                        $("#settings").dialog({
                            modal: true,
                            resizable: false,
                            draggable: false,
                            autoOpen: false,
                            width: 590,
                            height: 270,
                            buttons: {
                                Fechar: function() {
                                    $(this).dialog("close");
                                }
                            }
                        });
                        $('#grep').keyup(function(e) {
                            if(e.keyCode == 13) {
                                $( "#settings" ).dialog('close');
                            }
                        });
                        $("#open_settings").click(function(){
                            $( "#settings" ).dialog('open');
                            $("#open_settings").removeClass('ui-state-focus');
                        });
                        $(window).scroll(function(e) {
                            $('.float').css({
                                position: 'fixed',
                                top: '0',
                                left: 'auto'
                            });
                            documentHeight = $(document).height();
                            scrollPosition = $(window).height() + $(window).scrollTop();
                            scroll = (documentHeight <= scrollPosition ? true : false);
                        });
                        $(window).resize(function(){
                            if(scroll) {
                                scrollToBottom();
                            }
                        });
                        $("#grep").focus();
                        $("#open_settings").button();
                        setInterval ( "updateLog()", <?php echo $this->updateTime; ?> );
                        scrollToBottom();
                    });
                </script>
            </head>
            <body>
                <div id="settings" title="Configuração">
                    <p>Palavra chave:</p>
                    <input id="grep" type="text" value=""/>
                    <p>Negação?</p>
                    <div id="invert">
                        <input type="radio" value="1" id="invert1" name="invert" /><label for="invert1">Sim</label>
                        <input type="radio" value="0" id="invert2" name="invert" checked="checked" /><label for="invert2">Não</label>
                    </div>
                </div>
                <div class="float">
                    <button id="open_settings">Configuração</button>
                </div>
                <pre id="results"></pre>
            </body>
        </html>
        <?php
    }

}
