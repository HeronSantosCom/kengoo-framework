<?php

if (!function_exists("broken_magic_quotes")) {

    /**
     *
     * @param type $value
     * @param type $key
     */
    function broken_magic_quotes(&$value, $key) {
        $value = stripslashes($value);
    }

}

if (!class_exists('compose')) {

    /**
     *
     */
    class compose extends app {

        private $slashes = array("'", '"');
        private $discover = false;
        private $each_token = false;

        /**
         *
         */
        public function __construct() {
            ini_set('memory_limit', '96M');
            $this->each_token = certificate . "|";
            if (get_magic_quotes_gpc()) {
                $gpc = array(&$_GET, &$_POST, &$_COOKIE, &$_REQUEST);
                array_walk_recursive($gpc, 'broken_magic_quotes');
            }
        }

        /**
         *
         * @param type $name
         * @param type $arguments
         * @return boolean
         */
        public function __call($name, $arguments) {
            return false;
        }

        /**
         *
         * @param string $source
         * @return string
         */
        public function init($source, $encode = true) {
            //$this->stop($source);
            if (preg_match('/<!-- \((.*?)\) -->/', $source, $commands)) {
                $source = $this->init(str_replace($commands[0], $this->discover($commands[1], $encode), $source));
            } else {
                if (preg_match('/<!--(.*?)-->/', $source, $commands)) {
                    $call_function = false;
                    if (preg_match('/(.*?)@(.*)/', trim($commands[1]), $matches)) {
                        $call_function = $this->get_name_function($matches[1]);
                        $replace = $this->$call_function($matches[2]);
                        unset($call_function);
                    } else {
                        if (preg_match('/(.*)\((.*)\)(.*)/', trim($commands[1]), $matches)) {
                            $call_function = $this->get_name_function(trim($matches[1]));
                            $source = $this->$call_function($source, trim($matches[0]), trim($matches[2]), trim($matches[3]));
                            unset($call_function);
                        } else {
                            if (preg_match('/(\$|\^|\!)(.*)/', trim($commands[1]), $matches)) {
                                $call_function = $this->get_name_function($matches[1]);
                                $replace = $this->encode($this->$call_function($matches[2]), $encode);
                                unset($call_function);
                            }
                        }
                    }
                    if (isset($replace)) {
                        $this->discover = true;
                        $source = (!is_array($replace) ? $this->init(str_replace($commands[0], $replace, $source), $encode) : $replace);
                        unset($replace);
                    } else {
                        $source = $this->init(str_replace($commands[0], null, $source), $encode);
                    }
                }
                unset($commands);
            }
            if (ob_get_length()) ob_end_clean();
            return $source;
        }

        /**
         *
         * @param type $have
         * @return type
         */
        private function app($have) {
            ob_start();
            $instance = "cfc-" . md5($have);
            $class = explode("&", trim(str_replace(array("-", "/", ">"), " ", $have)));
            if (isset($class[1])) {
                $static = $class[1];
                $class = $class[0];
                call_user_func("{$class}::{$static}");
            } else {
                $class = explode(".", trim(str_replace(array("-", "/", ">"), " ", $have)));
                if (isset($class[1])) {
                    $static = $class[1];
                    $class = $class[0];
                    if (!isset($this->$instance)) {
                        $this->$instance = new $class();
                        if (knife::get_class_const($class, 'certificate') != certificate) {
                            trigger_error("The required application is not compatible with the platform...");
                            return die();
                        }
                    }
                    $this->$instance->$static();
                    unset($static);
                } else {
                    $class = $class[0];
                    if (!isset($this->$instance)) {
                        $this->$instance = new $class();
                        if (knife::get_class_const($class, 'certificate') != certificate) {
                            trigger_error("The required application is not compatible with the platform...");
                            return die();
                        }
                    }
                }
            }
            unset($instance, $class);
            return ob_get_clean();
        }

        /**
         *
         * @param type $have
         * @return type
         */
        private function session($have) {
            $sessions = explode(".", $have);
            $value = false;
            if (is_array($sessions)) {
                $size = sizeof($sessions);
                $keys = array_keys($sessions);
                for ($x = 0; $x < $size; $x++) {
                    $key = $keys[$x];
                    $session = $sessions[$key];
                    // ----
                    if (isset($value[$session])) {
                        $value = $value[$session];
                    } else {
                        if (isset($_SESSION[$session])) {
                            $value = $_SESSION[$session];
                        }
                    }
                    // ----
                    unset($key);
                }
                unset($size, $keys, $x);
            }
            unset($sessions, $have);
            return $value;
        }

        /**
         *
         * @param type $have
         * @return boolean
         */
        private function constant($have) {
            if (defined($have)) {
                return constant($have);
            }
            return false;
        }

        /**
         *
         * @param type $have
         * @return type
         */
        private function variable($have) {
            $value = false;
            $array = explode(".", $have);
            if (is_array($array)) {
                $node = array_shift($array);
                $value = $this->$node;
                if (count($array)) {
                    $size = sizeof($array);
                    $keys = array_keys($array);
                    for ($x = 0; $x < $size; $x++) {
                        $key = $keys[$x];
                        $node = $array[$key];
                        // ----
                        if (isset($value[$node])) {
                            $value = $value[$node];
                        }
                        // ----
                        unset($key);
                    }
                    unset($size, $keys, $x);
                }
            }
            return $value;
        }

        /**
         *
         */
        //TODO fazer com que o import funcione com declaração e variavel
        private function import($source, $function, $argument, $name) {
            unset($name);
            return str_replace("<!-- {$function} -->", knife::html($argument), $source);
        }

        /**
         *
         * @param type $source
         * @param type $function
         * @param type $argument
         * @param type $name
         * @return type
         */
        private function decode($source, $function, $argument, $name) {
            unset($name);
            return str_replace("<!-- {$function} -->", $this->discover($argument, false), $source);
        }

        /**
         *
         * @param type $source
         * @param type $function
         * @param type $argument
         * @param type $name
         * @return type
         */
        private function itself($source, $function, $argument, $name) {
            $init = strpos($source, "<!-- {$function} -->");
            $parser = substr($source, $init);
            $else = strpos($parser, "<!-- else {$name} -->");
            $end = strpos($parser, "<!-- end {$name} -->");
            $parser = substr($parser, 0, $end + strlen("<!-- end {$name} -->"));
            $parser_1 = substr($parser, strlen("<!-- {$function} -->"), $end);
            $parser_2 = null;
            if (strlen($else) > 0) {
                $parser_1 = substr($parser, strlen("<!-- {$function} -->"), $else - strlen("<!-- {$function} -->"));
                $parser_2 = substr($parser, $else + strlen("<!-- else {$name} -->"), $end - ($else + strlen("<!-- else {$name} -->")));
            }
            if (preg_match('/(.*)(\!\=|\=\=|\&\&|match|like|distinct|more|less|morethen|lessthen)(.*)/i', $argument, $condition)) { // .[\!\=|\=\=|\>|\>\=|\<|\<\=]|match|like|distinct|more|less
                if (isset($condition[3])) {
                    $x = $this->discover(trim($condition[1]));
                    $decision = trim($condition[2]);
                    $y = $this->discover(trim($condition[3]));
                    $offset = false;
                    switch ($decision) {
                        case '==': // igual
                            if ($x == $y) {
                                $offset = true;
                            }
                            break;
                        case '!=': // diferente
                            if ($x != $y) {
                                $offset = true;
                            }
                            break;
                        case '&&': // equivalente
                            if (preg_match("#($y)#", $x)) {
                                $offset = true;
                            }
                            break;
                        case 'less': // menor que
                            if ((double) $x < (double) $y) {
                                $offset = true;
                            }
                            break;
                        case 'more': // maior que
                            if ((double) $x > (double) $y) {
                                $offset = true;
                            }
                            break;
                        case 'lessthen': // menor ou igual que
                            if ((double) $x <= (double) $y) {
                                $offset = true;
                            }
                            break;
                        case 'morethen': // maior ou igual que
                            if ((double) $x >= (double) $y) {
                                $offset = true;
                            }
                            break;
                    }
                    $source = str_replace($parser, ($offset ? $parser_1 : $parser_2), $source);
                    unset($decision, $offset);
                }
            }
            unset($init, $parser, $else, $end, $parser_1, $parser_2, $condition);
            return $source;
        }

        /**
         *
         * @param type $source
         * @param type $function
         * @param type $argument
         * @param type $name
         * @return type
         */
        private function between($source, $function, $argument, $name) {
            $init = strpos($source, "<!-- {$function} -->");
            $parser = substr($source, $init);
            $end = strpos($parser, "<!-- end {$name} -->") + strlen("<!-- end {$name} -->");
            $parser = substr($parser, 0, $end);
            $offset = substr($parser, strlen("<!-- {$function} -->"), -strlen("<!-- end {$name} -->"));
            if (preg_match('/(.*),(.*)/i', $argument, $condition)) {
                if (isset($condition[2])) {
                    $start = $this->discover(trim($condition[1]));
                    $end = $this->discover(trim($condition[2]));
                    if ($end >= $start) {
                        $y = 0;
                        $replace = new SplFixedArray(($end - $start));
                        for ($x = $start; $x < $end; $x++) {
                            $t = $x + 1;
                            $replace[$y] = str_replace(array('<!-- $' . $name . '() -->', '$' . $name . '()'), $t, $offset);
                            $y++;
                        }
                        unset($start, $y);
                    }
                    $source = str_replace($parser, (isset($replace) ? join("", $replace->toArray()) : null), $source);
                    unset($start);
                    if (isset($replace)) {
                        unset($replace);
                    }
                }
            }
            unset($init, $parser, $end, $offset, $condition);
            return $source;
        }

        /**
         *
         * @param type $data
         * @return type
         */
        private function each_encode($data) {
            return @base64_encode($this->each_token . @serialize($data));
        }

        /**
         *
         * @param type $data
         * @return null
         */
        private function each_decode($data) {
            $data = trim(base64_decode($data));
            if (substr($data, 0, strlen($this->each_token)) == $this->each_token) {
                $data = substr($data, strlen($this->each_token));
                $data = @unserialize($data);
                if (is_array($data)) {
                    return $data;
                }
            }
            return null;
        }

        /**
         *
         * @param type $source
         * @param type $function
         * @param type $argument
         * @param type $name
         * @return type
         */
        private function each($source, $function, $argument, $name) {
            $init = strpos($source, "<!-- {$function} -->");
            $parser = substr($source, $init);
            $end = strpos($parser, "<!-- end {$name} -->") + strlen("<!-- end {$name} -->");
            $parser = substr($parser, 0, $end);
            $offset = substr($parser, strlen("<!-- {$function} -->"), -strlen("<!-- end {$name} -->"));
            $argument = trim($argument);
            if ($argument) {
                $sub = $this->each_decode($argument);
                $array = ($sub ? $sub : $this->discover(trim($argument)));
                if (is_array($array)) {
                    $size = sizeof($array);
                    $keys = array_keys($array);
                    $replace = new SplFixedArray($size);
                    for ($x = 0; $x < $size; $x++) {
                        $key = $keys[$x];
                        $value = $array[$key];
                        // ----
                        $replaced = str_replace(array('<!-- $' . $name . '() -->', '$' . $name . '(_key_)'), $key, $offset);
                        if (is_array($value)) {
                            $size_value = sizeof($value);
                            $keys_value = array_keys($value);
                            for ($y = 0; $y < $size_value; $y++) {
                                $field = $keys_value[$y];
                                $val = $value[$field];
                                // ----
                                $replaced = str_replace(array('<!-- $' . $name . '.' . $field . ' -->', '$' . $name . '(' . $field . ')'), (is_array($val) ? $this->each_encode($val) : htmlentities($val, ENT_COMPAT, encoding_source)), $replaced);
                                $replaced = str_replace(array('<!-- decode($' . $name . '.' . $field . ') -->', '<!-- decode ($' . $name . '.' . $field . ') -->'), (is_array($val) ? $this->each_encode($val) : $val), $replaced);
                                // ----
                                unset($field, $val);
                            }
                            unset($size_value, $keys_value, $y);
                        } else {
                            $replaced = str_replace(array('<!-- $' . $name . ' -->', '$' . $name . '()'), htmlentities($value, ENT_COMPAT, encoding_source), $replaced);
                            $replaced = str_replace(array('<!-- decode($' . $name . ') -->', '<!-- decode ($' . $name . ') -->'), $value, $replaced);
                        }
                        $replace[$x] = $this->init($replaced);
                        // ----
                        unset($key, $value, $replaced);
                    }
                    unset($size, $keys, $x);
                }
                unset($sub, $array);
            }
            $source = str_replace($parser, (isset($replace) ? join("", $replace->toArray()) : null), $source);
            unset($init, $parser, $end, $offset, $argument);
            if (isset($replace)) {
                unset($replace);
            }
            return $source;
        }

        /**
         *
         * @param type $needle
         * @param type $encode
         * @return type
         */
        private function discover($needle, $encode = true) {
            if (!is_array($needle)) {
                $this->discover = false;
                $source = $this->init("<!-- {$needle} -->", $encode);
                $needle = ($this->discover ? $source : $needle);
                if (!is_array($needle)) {
                    if (in_array(substr($needle, 0, 1), array("'", '"')) and in_array(substr($needle, -1), array("'", '"'))) {
                        $needle = substr(substr($needle, 0, (strlen($needle) - 1)), 1);
                    }
                }
                unset($source);
            }
            return $needle;
        }

        /**
         *
         * @param type $value
         * @param type $encode
         * @return type
         */
        private function encode($value, $encode = true) {
            if ($encode and !is_array($value)) {
                return htmlentities($value, ENT_COMPAT, encoding_source);
            }
            return $value;
        }

        /**
         *
         * @param type $parser
         * @return boolean
         */
        private function stop($parser) {
            if (strlen($parser) > 0) {
                $instance = "slc-" . md5($parser);
                if (isset($this->$instance)) {
                    if ($this->$instance > 3) { //Stop Looping Control
                        trigger_error("The system detected an illegal operation and blocked the execution ...");
                        return die();
                    }
                }
                return $this->$instance = ($this->$instance + 1);
            }
            return false;
        }

        /**
         *
         * @param type $name
         * @return string
         */
        private function get_name_function($name) {
            switch ($name) {
                case '^':
                    return "session";
                    break;
                case '!':
                    return "constant";
                    break;
                case '$':
                    return "variable";
                    break;
                case 'if':
                    return "itself";
                    break;
                case 'for':
                    return "between";
                    break;
            }
            return $name;
        }

    }

}