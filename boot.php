<?php

if (!class_exists('boot')) {

    /**
     *
     */
    class boot {

        /**
         *
         */
        public function __construct() {
            session_start();
            if ($this->install()) {
                setlocale(LC_ALL, timezone_locale);
                date_default_timezone_set(timezone_unix);
                if (!empty($_SERVER["HTTP_HOST"])) {
                    $source = new source();
                    if ($source->display) {
                        print $source->display;
                    }
                } else {
                    $args = (!empty($_SERVER["argv"]) ? $_SERVER["argv"] : false);
                    $call = (is_array($args) && !empty($args[1]) ? explode(".", $args[1], 2) : false);
                    if ($call) {
                        $class = $call[0];
                        $function = (!empty($call[1]) ? $call[1] : false);
                        unset($args[0], $args[1], $call);
                        sort($args);
                        define("permalink_path_split", serialize($args));
                        print "Welcome to project " . name . "\n";
                        print "Powered by " . sys . "\n";
                        print date("r") . ", calling {$class}" . ($function ? "->{$function}" : false) . " on pid " . getmypid() . "\n";
                        $ins = new $class();
                        $ins->$function();
                    } else {
                        trigger_error("É necessário informar a aplicação a ser executada.", E_USER_ERROR);
                    }
                }
            } else {
                trigger_error("Método de inicialização incorreta para continuar.", E_USER_ERROR);
            }
        }

        /**
         *
         * @return boolean
         */
        private function config() {
            $content = simplexml_load_file(configurations);
            if (isset($content->config["status"])) {
                define("certificate", session_id());
                define("name", trim((string) $content->config));
                define("status", (trim((string) $content->config["status"]) == "1" ? true : false));
                define("maintenance", (trim((string) $content->config["maintenance"]) == "1" ? true : false));
                define("permalink", (trim((string) $content->config["permalink"]) == "1" ? true : false));
                define("created", trim($content->information->date));
                define("subdomain", $this->subdomain($content));
                if (isset($content->defines)) {
                    config::export((array) $content->defines);
                    return true;
                }
            }
            return false;
        }

        /**
         *
         * @param type $content
         * @return boolean
         */
        private function subdomain($content) {
            if (isset($content->information)) {
                $domain = trim($content->information->domain);
                if (strpos(domain, $domain)) {
                    return trim(str_replace(".{$domain}", "", domain));
                }
            }
            return false;
        }

        /**
         *
         * @return boolean
         */
        private function install() {
            new install();
            if ($this->config()) {
                $_SESSION["__mimetypes__"] = array(
                    "application" => "application/octet-stream",
                    "ai" => "application/postscript",
                    "aif" => "audio/x-aiff",
                    "aifc" => "audio/x-aiff",
                    "aiff" => "audio/x-aiff",
                    "asc" => "text/plain",
                    "atom" => "application/atom+xml",
                    "au" => "audio/basic",
                    "avi" => "video/x-msvideo",
                    "bcpio" => "application/x-bcpio",
                    "bin" => "application/octet-stream",
                    "bmp" => "image/bmp",
                    "cdf" => "application/x-netcdf",
                    "cgm" => "image/cgm",
                    "class" => "application/octet-stream",
                    "cpio" => "application/x-cpio",
                    "cpt" => "application/mac-compactpro",
                    "csh" => "application/x-csh",
                    "css" => "text/css",
                    "dcr" => "application/x-director",
                    "dif" => "video/x-dv",
                    "dir" => "application/x-director",
                    "djv" => "image/vnd.djvu",
                    "djvu" => "image/vnd.djvu",
                    "dll" => "application/octet-stream",
                    "dmg" => "application/octet-stream",
                    "dms" => "application/octet-stream",
                    "doc" => "application/msword",
                    "dtd" => "application/xml-dtd",
                    "dv" => "video/x-dv",
                    "dvi" => "application/x-dvi",
                    "dxr" => "application/x-director",
                    "eps" => "application/postscript",
                    "etx" => "text/x-setext",
                    "exe" => "application/octet-stream",
                    "ez" => "application/andrew-inset",
                    "gif" => "image/gif",
                    "gram" => "application/srgs",
                    "grxml" => "application/srgs+xml",
                    "gtar" => "application/x-gtar",
                    "hdf" => "application/x-hdf",
                    "hqx" => "application/mac-binhex40",
                    "htm" => "text/html",
                    "html" => "text/html",
                    "ice" => "x-conference/x-cooltalk",
                    "ico" => "image/x-icon",
                    "ics" => "text/calendar",
                    "ief" => "image/ief",
                    "ifb" => "text/calendar",
                    "iges" => "model/iges",
                    "igs" => "model/iges",
                    "jnlp" => "application/x-java-jnlp-file",
                    "jp2" => "image/jp2",
                    "jpe" => "image/jpeg",
                    "jpeg" => "image/jpeg",
                    "jpg" => "image/jpeg",
                    "js" => "application/x-javascript",
                    "kar" => "audio/midi",
                    "latex" => "application/x-latex",
                    "lha" => "application/octet-stream",
                    "lzh" => "application/octet-stream",
                    "m3u" => "audio/x-mpegurl",
                    "m4a" => "audio/mp4a-latm",
                    "m4b" => "audio/mp4a-latm",
                    "m4p" => "audio/mp4a-latm",
                    "m4u" => "video/vnd.mpegurl",
                    "m4v" => "video/x-m4v",
                    "mac" => "image/x-macpaint",
                    "man" => "application/x-troff-man",
                    "mathml" => "application/mathml+xml",
                    "me" => "application/x-troff-me",
                    "mesh" => "model/mesh",
                    "mid" => "audio/midi",
                    "midi" => "audio/midi",
                    "mif" => "application/vnd.mif",
                    "mov" => "video/quicktime",
                    "movie" => "video/x-sgi-movie",
                    "mp2" => "audio/mpeg",
                    "mp3" => "audio/mpeg",
                    "mp4" => "video/mp4",
                    "mpe" => "video/mpeg",
                    "mpeg" => "video/mpeg",
                    "mpg" => "video/mpeg",
                    "mpga" => "audio/mpeg",
                    "ms" => "application/x-troff-ms",
                    "msh" => "model/mesh",
                    "mxu" => "video/vnd.mpegurl",
                    "nc" => "application/x-netcdf",
                    "oda" => "application/oda",
                    "ogg" => "application/ogg",
                    "pbm" => "image/x-portable-bitmap",
                    "pct" => "image/pict",
                    "pdb" => "chemical/x-pdb",
                    "pdf" => "application/pdf",
                    "pgm" => "image/x-portable-graymap",
                    "pgn" => "application/x-chess-pgn",
                    "pic" => "image/pict",
                    "pict" => "image/pict",
                    "png" => "image/png",
                    "pnm" => "image/x-portable-anymap",
                    "pnt" => "image/x-macpaint",
                    "pntg" => "image/x-macpaint",
                    "ppm" => "image/x-portable-pixmap",
                    "ppt" => "application/vnd.ms-powerpoint",
                    "ps" => "application/postscript",
                    "qt" => "video/quicktime",
                    "qti" => "image/x-quicktime",
                    "qtif" => "image/x-quicktime",
                    "ra" => "audio/x-pn-realaudio",
                    "ram" => "audio/x-pn-realaudio",
                    "ras" => "image/x-cmu-raster",
                    "rdf" => "application/rdf+xml",
                    "rgb" => "image/x-rgb",
                    "rm" => "application/vnd.rn-realmedia",
                    "roff" => "application/x-troff",
                    "rtf" => "text/rtf",
                    "rtx" => "text/richtext",
                    "sgm" => "text/sgml",
                    "sgml" => "text/sgml",
                    "sh" => "application/x-sh",
                    "shar" => "application/x-shar",
                    "silo" => "model/mesh",
                    "sit" => "application/x-stuffit",
                    "skd" => "application/x-koan",
                    "skm" => "application/x-koan",
                    "skp" => "application/x-koan",
                    "skt" => "application/x-koan",
                    "smi" => "application/smil",
                    "smil" => "application/smil",
                    "snd" => "audio/basic",
                    "so" => "application/octet-stream",
                    "spl" => "application/x-futuresplash",
                    "src" => "application/x-wais-source",
                    "sv4cpio" => "application/x-sv4cpio",
                    "sv4crc" => "application/x-sv4crc",
                    "svg" => "image/svg+xml",
                    "swf" => "application/x-shockwave-flash",
                    "t" => "application/x-troff",
                    "tar" => "application/x-tar",
                    "tcl" => "application/x-tcl",
                    "tex" => "application/x-tex",
                    "texi" => "application/x-texinfo",
                    "texinfo" => "application/x-texinfo",
                    "tif" => "image/tiff",
                    "tiff" => "image/tiff",
                    "tr" => "application/x-troff",
                    "tsv" => "text/tab-separated-values",
                    "txt" => "text/plain",
                    "ustar" => "application/x-ustar",
                    "vcd" => "application/x-cdlink",
                    "vrml" => "model/vrml",
                    "vxml" => "application/voicexml+xml",
                    "wav" => "audio/x-wav",
                    "wbmp" => "image/vnd.wap.wbmp",
                    "wbmxl" => "application/vnd.wap.wbxml",
                    "wml" => "text/vnd.wap.wml",
                    "wmlc" => "application/vnd.wap.wmlc",
                    "wmls" => "text/vnd.wap.wmlscript",
                    "wmlsc" => "application/vnd.wap.wmlscriptc",
                    "wrl" => "model/vrml",
                    "xbm" => "image/x-xbitmap",
                    "xht" => "application/xhtml+xml",
                    "xhtml" => "application/xhtml+xml",
                    "xls" => "application/vnd.ms-excel",
                    "xml" => "application/xml",
                    "xpm" => "image/x-xpixmap",
                    "xsl" => "application/xml",
                    "xslt" => "application/xslt+xml",
                    "xul" => "application/vnd.mozilla.xul+xml",
                    "xwd" => "image/x-xwindowdump",
                    "xyz" => "chemical/x-xyz",
                    "zip" => "application/zip",
                    "dot" => "application/msword",
                    "wrd" => "application/msword",
                    "pgp" => "application/pgp",
                    "tgz" => "application/x-gtar",
                    "gz" => "application/x-gzip",
                    "php" => "application/x-httpd-php",
                    "php3" => "application/x-httpd-php",
                    "ppd" => "application/x-photoshop",
                    "psd" => "application/x-photoshop",
                    "swc" => "application/x-shockwave-flash",
                    "rf" => "application/x-shockwave-flash",
                    //"ra" => "audio/x-realaudio",
                    //"wav" => "audio/wav",
                    //"bmp" => "image/bitmap",
                    "iff" => "image/iff",
                    "jb2" => "image/jb2",
                    "jpx" => "image/jpx",
                    //"wbmp" => "image/vnd.wap.wbmp",
                    //"xbm" => "image/xbm",
                    //"avi" => "video/x-ms-video",
                    "eml" => "message/rfc822"
                );
                return true;
            }
            return false;
        }

    }

}